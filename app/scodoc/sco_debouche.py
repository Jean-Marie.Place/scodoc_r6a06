# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@gmail.com
#
##############################################################################

"""
Rapport (table) avec dernier semestre fréquenté et débouché de chaque étudiant
"""
from flask import g, render_template, request, url_for

from app.comp import res_sem
from app.comp.res_compat import NotesTableCompat
from app.models import Identite
import app.scodoc.sco_utils as scu
import app.scodoc.notesdb as ndb
from app.scodoc.gen_tables import GenTable
from app.scodoc import sco_preferences
import sco_version


def report_debouche_date(start_year=None, fmt="html"):
    """Rapport (table) pour les débouchés des étudiants sortis
    à partir de l'année indiquée.
    """
    if not start_year:
        return report_debouche_ask_date("Année de début de la recherche")
    else:
        try:
            start_year = int(start_year)
        except ValueError:
            return report_debouche_ask_date(
                "Année invalide. Année de début de la recherche"
            )

    if fmt == "xls":
        keep_numeric = True  # pas de conversion des notes en strings
    else:
        keep_numeric = False

    etudids = get_etudids_with_debouche(start_year)
    tab = table_debouche_etudids(etudids, keep_numeric=keep_numeric)

    tab.filename = scu.make_filename(f"debouche_scodoc_{start_year}")
    tab.origin = f"Généré par {sco_version.SCONAME} le {scu.timedate_human_repr()}"
    tab.caption = f"Récapitulatif débouchés à partir du 1/1/{start_year}."
    tab.base_url = f"{request.base_url}?start_year={start_year}"
    return tab.make_page(
        title="""<h2 class="formsemestre">Débouchés étudiants </h2>""",
        page_title="Débouchés étudiants",
        fmt=fmt,
        with_html_headers=True,
        template="sco_page_dept.j2",
    )


def get_etudids_with_debouche(start_year):
    """Liste des etudids de tous les semestres terminant
    à partir du 1er janvier de start_year
    et ayant un 'debouche' renseigné.
    """
    start_date = str(start_year) + "-01-01"
    # Recupere tous les etudid avec un debouché renseigné et une inscription dans un semestre
    # posterieur à la date de depart:
    # r = ndb.SimpleDictFetch(
    #                    """SELECT DISTINCT i.etudid
    #                    FROM notes_formsemestre_inscription i, admissions adm, notes_formsemestre s
    #                    WHERE adm.debouche is not NULL
    #                    AND i.etudid = adm.etudid AND i.formsemestre_id = s.formsemestre_id
    #                    AND s.date_fin >= %(start_date)s
    #                    """,
    #                    {'start_date' : start_date })

    r = ndb.SimpleDictFetch(
        """SELECT DISTINCT i.etudid
        FROM notes_formsemestre_inscription i, notes_formsemestre s, itemsuivi it
        WHERE i.etudid = it.etudid
        AND i.formsemestre_id = s.id AND s.date_fin >= %(start_date)s
        AND s.dept_id = %(dept_id)s
        """,
        {"start_date": start_date, "dept_id": g.scodoc_dept_id},
    )

    return [x["etudid"] for x in r]


def table_debouche_etudids(etudids, keep_numeric=True):
    """Rapport pour ces étudiants"""
    rows = []
    # Recherche les débouchés:
    # itemsuivi_etuds = {etudid: itemsuivi_list_etud(etudid) for etudid in etudids}
    all_tags = set()
    for etudid in etudids:
        etud = Identite.get_etud(etudid)
        # collecte les tags
        all_tags.update(tag.title for item in etud.itemsuivis for tag in item.tags)
        # retrouve le "dernier" semestre (au sens de la date de fin)
        formsemestres = etud.get_formsemestres()
        dates_fin = [s.date_fin for s in formsemestres]
        imax = dates_fin.index(max(dates_fin))
        formsemestre = formsemestres[imax]
        nt: NotesTableCompat = res_sem.load_formsemestre_results(formsemestre)
        row = {
            "etudid": etudid,
            "civilite": etud.civilite,
            "nom": etud.nom,
            "prenom": etud.prenom,
            "_nom_target": url_for(
                "scolar.fiche_etud", scodoc_dept=g.scodoc_dept, etudid=etudid
            ),
            "_prenom_target": url_for(
                "scolar.fiche_etud", scodoc_dept=g.scodoc_dept, etudid=etudid
            ),
            "_nom_td_attrs": f'id="{etudid}" class="etudinfo"',
            # 'debouche' : etud['debouche'],
            "moy": scu.fmt_note(nt.get_etud_moy_gen(etudid), keep_numeric=keep_numeric),
            "rang": nt.get_etud_rang(etudid),
            "effectif": len(nt.T),
            "semestre_id": formsemestre.semestre_id,
            "semestre": formsemestre.titre,
            "date_debut": formsemestre.date_debut,
            "date_fin": formsemestre.date_fin,
            "periode": f"{formsemestre.mois_debut()} - {formsemestre.mois_fin()}",
            "sem_ident": f"{formsemestre.date_debut.isoformat()} {formsemestre.titre}",  # utile pour tris
        }
        # recherche des débouchés
        itemsuivis = etud.itemsuivis  # liste de plusieurs items
        if itemsuivis:
            if keep_numeric:  # pour excel:
                row["debouche"] = "\n".join(
                    f"""{it.item_date.strftime(scu.DATE_FMT) if it.item_date else ""
                    } : {it.situation or ""}"""
                    for it in itemsuivis
                )
            else:
                row["debouche"] = "<br>".join(
                    [
                        f"""{it.item_date.strftime(scu.DATE_FMT) if it.item_date else ""
                        } : {it.situation or ""}
                        <i>{', '.join( tag.title for tag in it.tags)}</i>
                        """
                        for it in itemsuivis
                    ]
                )
            for it in itemsuivis:
                for tag in it.tags:
                    tag_name = tag.title.strip()
                    row[f"tag_{tag_name}"] = tag_name
        else:
            row["debouche"] = "non renseigné"
        rows.append(row)
    rows.sort(key=lambda x: x["sem_ident"])

    titles = {
        "civilite": "",
        "nom": "Nom",
        "prenom": "Prénom",
        "semestre": "Dernier semestre",
        "semestre_id": "S",
        "periode": "Dates",
        "moy": "Moyenne",
        "rang": "Rang",
        "effectif": "Eff.",
        "debouche": "Débouché",
    }
    columns_ids = [
        "semestre",
        "semestre_id",
        "periode",
        "civilite",
        "nom",
        "prenom",
        "moy",
        "rang",
        "effectif",
        "debouche",
    ]
    for tag in all_tags:
        titles[f"tag_{tag}"] = tag
        columns_ids.append(f"tag_{tag}")
    tab = GenTable(
        columns_ids=columns_ids,
        titles=titles,
        rows=rows,
        # html_col_width='4em',
        html_sortable=True,
        html_class="table_leftalign table_listegroupe",
        preferences=sco_preferences.SemPreferences(),
        table_id="table_debouche_etudids",
    )
    return tab


def report_debouche_ask_date(msg: str) -> str:
    """Formulaire demande date départ"""
    return render_template(
        "sco_page_dept.j2",
        content=f"""
    <h2>Table des débouchés des étudiants</h2>
    <form method="GET">
    {msg}
    <input type="text" name="start_year" value="" size=10/>
    </form>
    """,
    )
