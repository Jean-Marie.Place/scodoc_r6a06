# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Génération de la "sidebar" (marge gauche des pages HTML)
"""

from flask import request

from app import db
from app.models import Evaluation, GroupDescr, ModuleImpl, Partition


def retreive_formsemestre_from_request() -> int:
    """Cherche si on a de quoi déduire le semestre affiché à partir des
    arguments de la requête:
    formsemestre_id ou moduleimpl ou evaluation ou group_id ou partition_id
    Returns None si pas défini.
    """
    if request.method == "GET":
        args = request.args
    elif request.method == "POST":
        args = request.form
    else:
        return None
    formsemestre_id = None
    # Search formsemestre
    group_ids = args.get("group_ids", [])
    if "formsemestre_id" in args:
        formsemestre_id = args["formsemestre_id"]
    elif "moduleimpl_id" in args and args["moduleimpl_id"]:
        modimpl = db.session.get(ModuleImpl, args["moduleimpl_id"])
        if not modimpl:
            return None  # suppressed ?
        formsemestre_id = modimpl.formsemestre_id
    elif "evaluation_id" in args:
        evaluation = db.session.get(Evaluation, args["evaluation_id"])
        if not evaluation:
            return None  # evaluation suppressed ?
        formsemestre_id = evaluation.moduleimpl.formsemestre_id
    elif "group_id" in args:
        group = db.session.get(GroupDescr, args["group_id"])
        if not group:
            return None
        formsemestre_id = group.partition.formsemestre_id
    elif group_ids:
        if isinstance(group_ids, str):
            group_ids = group_ids.split(",")
        group_id = group_ids[0]
        group = db.session.get(GroupDescr, group_id)
        if not group:
            return None
        formsemestre_id = group.partition.formsemestre_id
    elif "partition_id" in args:
        partition = db.session.get(Partition, args["partition_id"])
        if not partition:
            return None
        formsemestre_id = partition.formsemestre_id

    if formsemestre_id is None:
        return None  # no current formsemestre
    try:
        return int(formsemestre_id)
    except ValueError:
        return None  # no current formsemestre
