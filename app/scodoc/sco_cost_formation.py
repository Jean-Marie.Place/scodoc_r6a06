# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Rapports estimation coût de formation basé sur le programme pédagogique
   et les nombres de groupes.

   (coût théorique en heures équivalent TD)
"""
from flask import request, Response

from app.models import FormSemestre
from app.scodoc import sco_preferences
from app.scodoc.gen_tables import GenTable
from app.scodoc.sco_exceptions import ScoValueError
import app.scodoc.sco_utils as scu
import sco_version


def formsemestre_table_estim_cost(
    formsemestre: FormSemestre,
    n_group_td=1,
    n_group_tp=1,
    coef_tp=1,
    coef_cours=1.5,
):
    """
    Rapports estimation coût de formation basé sur le programme pédagogique
    et les nombres de groupes.
    Coût théorique en heures équivalent TD.
    Attention: ne prend en compte que les modules utilisés dans ce semestre.
    Attention: prend en compte _tous_ les modules utilisés dans ce semestre, ce qui
    peut conduire à une sur-estimation du coût s'il y a des modules optionnels
    (dans ce cas, retoucher le tableau excel exporté).
    """
    rows = []
    for modimpl in formsemestre.modimpls:
        rows.append(
            {
                "code": modimpl.module.code or "",
                "titre": modimpl.module.titre,
                "heures_cours": modimpl.module.heures_cours or 0.0,
                "heures_td": (modimpl.module.heures_td or 0.0) * n_group_td,
                "heures_tp": (modimpl.module.heures_tp or 0.0) * n_group_tp,
            }
        )

    # calcul des heures:
    for row in rows:
        row["HeqTD"] = (
            row["heures_td"]
            + coef_cours * row["heures_cours"]
            + coef_tp * row["heures_tp"]
        )
    sum_cours = sum(t["heures_cours"] for t in rows)
    sum_td = sum(t["heures_td"] for t in rows)
    sum_tp = sum(t["heures_tp"] for t in rows)
    sum_heqtd = sum_td + coef_cours * sum_cours + coef_tp * sum_tp
    # assert abs(sum(t["HeqTD"] for t in rows) - sum_heqtd) < 0.01, "%s != %s" % (
    #     sum(t["HeqTD"] for t in rows),
    #     sum_heqtd,
    # )

    rows.append(
        {
            "code": "TOTAL SEMESTRE",
            "heures_cours": sum_cours,
            "heures_td": sum_td,
            "heures_tp": sum_tp,
            "HeqTD": sum_heqtd,
            "_table_part": "foot",
        }
    )

    titles = {
        "code": "Code",
        "titre": "Titre",
        "heures_cours": "Cours",
        "heures_td": "TD",
        "heures_tp": "TP",
        "HeqTD": "HeqTD",
    }

    tab = GenTable(
        titles=titles,
        columns_ids=(
            "code",
            "titre",
            "heures_cours",
            "heures_td",
            "heures_tp",
            "HeqTD",
        ),
        rows=rows,
        html_sortable=True,
        preferences=sco_preferences.SemPreferences(formsemestre.id),
        html_class="table_leftalign table_listegroupe",
        xls_before_table=[
            [formsemestre.titre_annee()],
            [
                f"Formation {formsemestre.formation.titre} version {formsemestre.formation.version}"
            ],
            [],
            ["", "TD", "TP"],
            ["Nombre de groupes", n_group_td, n_group_tp],
            [],
            [],
        ],
        html_caption="""<div class="help">
                    Estimation du coût de formation basé sur le programme pédagogique
    et les nombres de groupes.<br>
    Coût théorique en heures équivalent TD.<br>
    Attention: ne prend en compte que les modules utilisés dans ce semestre.<br>
    Attention: prend en compte <em>tous les modules</em> utilisés dans ce semestre, ce qui
    peut conduire à une sur-estimation du coût s'il y a des modules optionnels
    (dans ce cas, retoucher le tableau excel exporté).
    </div>
                    """,
        origin=f"""Généré par {sco_version.SCONAME} le {scu.timedate_human_repr()}""",
        filename=f"EstimCout-S{formsemestre.semestre_id}",
        table_id="formsemestre_table_estim_cost",
    )
    return tab


# view
def formsemestre_estim_cost(
    formsemestre_id: int,
    n_group_td: int | str = 1,
    n_group_tp: int | str = 1,
    coef_tp: float | str = 1.0,
    coef_cours: float | str = 1.5,
    fmt="html",
) -> str | Response:
    """Page (formulaire) estimation coûts"""
    try:
        n_group_td = int(n_group_td)
        n_group_tp = int(n_group_tp)
        coef_tp = float(coef_tp)
        coef_cours = float(coef_cours)
    except ValueError as exc:
        raise ScoValueError("paramètre invalide: utiliser des nombres") from exc

    formsemestre = FormSemestre.get_formsemestre(formsemestre_id)

    tab = formsemestre_table_estim_cost(
        formsemestre,
        n_group_td=n_group_td,
        n_group_tp=n_group_tp,
        coef_tp=coef_tp,
        coef_cours=coef_cours,
    )
    tab.html_before_table = f"""
    <form name="f" method="get" action="{request.base_url}">
    <input type="hidden" name="formsemestre_id" value="{formsemestre.id}"></input>
    Nombre de groupes de TD: <input type="text" name="n_group_td" value="{n_group_td}" onchange="document.f.submit()"/><br>
    Nombre de groupes de TP: <input type="text" name="n_group_tp" value="{n_group_tp}" onchange="document.f.submit()"/>
    &nbsp;Coefficient heures TP: <input type="text" name="coef_tp" value="{coef_tp}" onchange="document.f.submit()"/>
    <br>
    </form>
    """
    tab.base_url = "%s?formsemestre_id=%s&n_group_td=%s&n_group_tp=%s&coef_tp=%s" % (
        request.base_url,
        formsemestre.id,
        n_group_td,
        n_group_tp,
        coef_tp,
    )

    return tab.make_page(fmt=fmt)
