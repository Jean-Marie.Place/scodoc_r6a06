# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Exports groupes
"""

from app.scodoc import notesdb as ndb
from app.scodoc import sco_groups_view
from app.scodoc import sco_preferences
from app.scodoc.gen_tables import GenTable
import app.scodoc.sco_utils as scu
import sco_version


def groups_list_annotation(group_ids: list[int]) -> list[dict]:
    """Renvoie la liste des annotations pour les groupes d"étudiants indiqués
    Arg: liste des id de groupes
    Clés: etudid, ine, nip, nom, prenom, date, comment
    """
    cnx = ndb.GetDBConnexion()
    cursor = cnx.cursor(cursor_factory=ndb.ScoDocCursor)
    annotations = []
    for group_id in group_ids:
        cursor.execute(
            """SELECT i.id AS etudid, i.code_nip, i.code_ine, i.nom, i.prenom, ea.date, ea.comment
            FROM group_membership gm, identite i, etud_annotations ea
            WHERE gm.group_id=%(group_ids)s
            AND gm.etudid=i.id
            AND i.id=ea.etudid
            """,
            {"group_ids": group_id},
        )
        annotations += cursor.dictfetchall()
    return annotations


def groups_export_annotations(group_ids, formsemestre_id=None, fmt="html"):
    """Les annotations"""
    groups_infos = sco_groups_view.DisplayedGroupsInfos(
        group_ids, formsemestre_id=formsemestre_id
    )
    annotations = groups_list_annotation(groups_infos.group_ids)
    for annotation in annotations:
        annotation["date_str"] = annotation["date"].strftime(scu.DATEATIME_FMT)
    if fmt == "xls":
        columns_ids = ("etudid", "nom", "prenom", "date", "comment")
    else:
        columns_ids = ("etudid", "nom", "prenom", "date_str", "comment")
    table = GenTable(
        rows=annotations,
        columns_ids=columns_ids,
        titles={
            "etudid": "etudid",
            "nom": "Nom",
            "prenom": "Prénom",
            "date": "Date",
            "date_str": "Date",
            "comment": "Annotation",
        },
        origin=f"Généré par {sco_version.SCONAME} le {scu.timedate_human_repr()}",
        page_title=f"Annotations sur les étudiants de {groups_infos.groups_titles}",
        caption="Annotations",
        base_url=groups_infos.base_url,
        html_sortable=True,
        html_class="table_leftalign",
        preferences=sco_preferences.SemPreferences(formsemestre_id),
        table_id="groups_export_annotations",
    )
    return table.make_page(fmt=fmt)
