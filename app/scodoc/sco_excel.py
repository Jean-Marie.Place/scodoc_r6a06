# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################


"""Excel file handling"""

import datetime
import io
import time
from enum import Enum
from tempfile import NamedTemporaryFile
from typing import AnyStr

import openpyxl.utils.datetime
from openpyxl.styles.numbers import FORMAT_NUMBER_00, FORMAT_GENERAL, FORMAT_DATE_DDMMYY
from openpyxl.comments import Comment
from openpyxl import Workbook, load_workbook
from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import Font, Border, Side, Alignment, PatternFill
from openpyxl.worksheet.worksheet import Worksheet

import app.scodoc.sco_utils as scu
from app import log
from app.models.scolar_event import ScolarEvent
from app.scodoc.sco_exceptions import ScoValueError
from app.scodoc import notesdb, sco_preferences


class COLORS(Enum):
    BLACK = "FF000000"
    WHITE = "FFFFFFFF"
    RED = "FFFF0000"
    BROWN = "FF993300"
    PURPLE = "FF993366"
    BLUE = "FF0000FF"
    ORANGE = "FFFF3300"
    LIGHT_YELLOW = "FFFFFF99"


# Un style est enregistré comme un dictionnaire avec des attributs dans la liste suivante:
# font, border, number_format, fill,...
# (cf https://openpyxl.readthedocs.io/en/stable/styles.html#working-with-styles)


def xldate_as_datetime(xldate):
    """Conversion d'une date Excel en datetime python
    Deux formats de chaîne acceptés:
     * JJ/MM/YYYY (chaîne naïve)
     * Date ISO (valeur de type date lue dans la feuille)
    Peut lever une ValueError
    """
    try:
        return datetime.datetime.strptime(xldate, scu.DATE_FMT)
    except:
        return openpyxl.utils.datetime.from_ISO8601(xldate)


def adjust_sheetname(sheet_name):
    """Renvoie un nom convenable pour une feuille excel: < 31 cars, sans caractères spéciaux
    Le / n'est pas autorisé par exemple.
    Voir https://xlsxwriter.readthedocs.io/workbook.html#add_worksheet
    """
    sheet_name = scu.make_filename(sheet_name)
    # Le nom de la feuille ne peut faire plus de 31 caractères.
    # si la taille du nom de feuille est > 31 on tronque (on pourrait remplacer par 'feuille' ?)
    return sheet_name[:31]


class ScoExcelBook:
    """Permet la génération d'un classeur xlsx composé de plusieurs feuilles.
    usage:
        wb = ScoExcelBook()
        ws0 = wb.create_sheet('sheet name 0')
        ws1 = wb.create_sheet('sheet name 1')
        ...
        steam = wb.generate()
    """

    def __init__(self):
        self.sheets = []  # list of sheets
        self.wb = Workbook(write_only=True)

    def create_sheet(self, sheet_name="feuille", default_style=None):
        """Crée une nouvelle feuille dans ce classeur
        sheet_name -- le nom de la feuille
        default_style -- le style par défaut
        """
        sheet_name = adjust_sheetname(sheet_name)
        ws = self.wb.create_sheet(sheet_name)
        sheet = ScoExcelSheet(sheet_name, default_style, ws)
        self.sheets.append(sheet)
        return sheet

    def generate(self):
        """génération d'un stream binaire représentant la totalité du classeur.
        retourne le flux
        """
        for sheet in self.sheets:
            sheet.prepare()
        # construction d'un flux
        # (https://openpyxl.readthedocs.io/en/stable/tutorial.html#saving-as-a-stream)
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            return tmp.read()


def excel_make_style(
    bold=False,
    italic=False,
    outline=False,
    color: COLORS = COLORS.BLACK,
    bgcolor: COLORS = None,
    halign=None,
    valign=None,
    number_format=None,
    font_name="Arial",
    size=10,
):
    """Contruit un style.
    Les couleurs peuvent être spécfiées soit par une valeur de COLORS,
    soit par une chaine argb (exple "FF00FF00" pour le vert)
    color -- La couleur du texte
    bgcolor -- la couleur de fond
    halign -- alignement horizontal ("left", "right", "center")
    valign -- alignement vertical ("top", "bottom", "center")
    number_format -- formattage du contenu ("General", "@", ...)
    font_name -- police
    size -- taille de police
    """
    style = {}
    font = Font(
        name=font_name,
        bold=bold,
        italic=italic,
        outline=outline,
        color=color.value,
        size=size,
    )
    style["font"] = font
    if bgcolor:
        style["fill"] = PatternFill(fill_type="solid", fgColor=bgcolor.value)
    if halign or valign:
        al = Alignment()
        if halign:
            al.horizontal = {
                "left": "left",
                "right": "right",
                "center": "center",
            }[halign]
        if valign:
            al.vertical = {
                "top": "top",
                "bottom": "bottom",
                "center": "center",
            }[valign]
        style["alignment"] = al
    if number_format is None:
        style["number_format"] = FORMAT_GENERAL
    else:
        style["number_format"] = number_format
    return style


class ScoExcelSheet:
    """Représente une feuille qui peut être indépendante ou intégrée dans un ScoExcelBook.
    En application des directives de la bibliothèque sur l'écriture optimisée,
    l'ordre des opérations est imposé:
    * instructions globales (largeur/maquage des colonnes et ligne, ...)
    * construction et ajout des cellules et ligne selon le sens de lecture (occidental)
    ligne de haut en bas et cellules de gauche à droite (i.e. A1, A2, .. B1, B2, ..)
    * pour finir appel de la méthode de génération
    """

    def __init__(self, sheet_name="feuille", default_style=None, wb: Workbook = None):
        """Création de la feuille. sheet_name
        -- le nom de la feuille default_style
        -- le style par défaut des cellules ws
        -- None si la feuille est autonome (elle crée son propre wb), sinon c'est la worksheet
        créée par le workbook propriétaire un workbook est créé et associé à cette feuille.
        """
        # Le nom de la feuille ne peut faire plus de 31 caractères.
        # si la taille du nom de feuille est > 31 on tronque (on pourrait remplacer par 'feuille' ?)
        self.sheet_name = adjust_sheetname(sheet_name)
        if default_style is None:
            default_style = excel_make_style()
        self.default_style = default_style
        if wb is None:
            self.wb = Workbook()
            self.ws = self.wb.active
            self.ws.title = self.sheet_name
        else:
            self.wb = None
            self.ws = wb
        # internal data
        self.rows = []  # list of list of cells
        self.column_dimensions = {}
        self.row_dimensions = {}
        self.auto_filter = None

    def excel_make_composite_style(
        self,
        alignment=None,
        border=None,
        fill=None,
        number_format=None,
        font=None,
    ) -> dict:
        "création d'un dict"
        style = {}
        if font is not None:
            style["font"] = font
        if alignment is not None:
            style["alignment"] = alignment
        if border is not None:
            style["border"] = border
        if fill is not None:
            style["fill"] = fill
        if number_format is None:
            style["number_format"] = FORMAT_GENERAL
        else:
            style["number_format"] = number_format
        return style

    @staticmethod
    def i2col(idx: int | str) -> str:
        "traduit un index ou lettre de colonne en lettre de colonne"
        if isinstance(idx, str):
            return idx
        if idx < 26:  # one letter key
            return chr(idx + 65)
        # two letters AA..ZZ
        first = (idx // 26) + 64
        second = (idx % 26) + 65
        return "" + chr(first) + chr(second)

    def set_column_dimension_width(self, cle=None, value: int | str | list = 21):
        """Détermine la largeur d'une colonne.
        cle -- identifie la colonne (lettre ou indice à partir de 0),
        Si cle is None, affecte toutes les colonnes.

        value est soit la liste des largeurs de colonnes [ (0, width0), (1, width1), ...]
        soit la largeur de la colonne indiquée par cle, soit "auto".
        Largeurs en unité : 7 pixels comme affiché dans Excel)
        ou value == "auto", ajuste la largeur au contenu
        """
        if cle is None:
            cols_widths = enumerate(value)
        else:
            cols_widths = [(cle, value)]

        for idx, width in cols_widths:
            if width == "auto":
                self.adjust_column_widths(column_letter=self.i2col(idx))
            else:
                self.ws.column_dimensions[self.i2col(idx)].width = width

    def set_row_dimension_height(self, cle=None, value=21):
        """Détermine la hauteur d'une ligne. cle -- identifie la ligne (1, 2, ...) si None,
        value donne la liste des hauteurs de colonnes depuis 1, 2, 3, ... value -- la dimension
        """
        if cle is None:
            for i, val in enumerate(value, start=1):
                self.ws.row_dimensions[i].height = val
            # No keys: value is a list of widths
        else:
            self.ws.row_dimensions[cle].height = value

    def set_row_dimension_hidden(self, cle, value):
        """Masque ou affiche une ligne.
        cle -- identifie la colonne (1...)
        value -- boolean (vrai = colonne cachée)
        """
        self.ws.row_dimensions[cle].hidden = value

    def set_column_dimension_hidden(self, cle, value):
        """Masque ou affiche une colonne.
        cle -- identifie la colonne (1...)
        value -- boolean (vrai = colonne cachée)
        """
        self.ws.column_dimensions[cle].hidden = value

    def set_auto_filter(self, filter_range):
        "met en place un auto-filter excel: le range désigne les cellules de titres"
        self.auto_filter = filter_range

    def adjust_column_widths(
        self, column_letter=None, min_row=None, max_row=None, min_col=None, max_col=None
    ):
        """Adjust columns widths to fit their content.
        If column_letter, adjust only this column, else adjust all.
        (min_row, max_row, min_col, max_col) can be used to restrinct the area to consider
        while determining the widths.
        """
        # Create a dictionary to store the maximum width of each column
        col_widths = {}

        if column_letter is None:
            # Iterate over each row and cell in the worksheet
            for row in self.ws.iter_rows(
                min_row=min_row, max_row=max_row, min_col=min_col, max_col=max_col
            ):
                for cell in row:
                    # Get the length of the cell value (converted to string)
                    cell_value = str(cell.value)
                    # Update the maximum width for the column
                    col_widths[cell.column_letter] = max(
                        col_widths.get(cell.column_letter, 0), len(cell_value)
                    )
        else:
            min_row = self.ws.min_row if min_row is None else min_row
            max_row = self.ws.max_row if max_row is None else max_row

            for row in range(min_row, max_row + 1):
                cell = self.ws[f"{column_letter}{row}"]
                cell_value = str(cell.value)
                col_widths[cell.column_letter] = max(
                    col_widths.get(cell.column_letter, 0), len(cell_value)
                )

        # Set the column widths based on the maximum length found
        # (nb: the width is expressed in characters, in the default font)
        for col, width in col_widths.items():
            self.ws.column_dimensions[col].width = width

    def make_cell(
        self, value: any = None, style: dict = None, comment=None
    ) -> WriteOnlyCell:
        """Construit une cellule.
        value -- contenu de la cellule (texte, numérique, booléen ou date)
        style -- style par défaut (dictionnaire cf. excel_make_style) de la feuille si non spécifié
        """
        # adaptation des valeurs si nécessaire
        if value is None:
            value = ""
        elif value is True:
            value = 1
        elif value is False:
            value = 0
        elif isinstance(value, datetime.datetime):
            # make date naive (cf https://openpyxl.readthedocs.io/en/latest/datetime.html#timezones)
            value = value.replace(tzinfo=None)

        # création de la cellule
        cell = WriteOnlyCell(self.ws, value)

        # recopie des styles
        if style is None:
            style = self.default_style
        if "font" in style:
            cell.font = style["font"]
        if "alignment" in style:
            cell.alignment = style["alignment"]
        if "border" in style:
            cell.border = style["border"]
        if "fill" in style:
            cell.fill = style["fill"]
        if "number_format" in style:
            cell.number_format = style["number_format"]
        if "fill" in style:
            cell.fill = style["fill"]
        if "alignment" in style:
            cell.alignment = style["alignment"]
        if not comment is None:
            cell.comment = Comment(comment, "scodoc")
            lines = comment.splitlines()
            cell.comment.width = 7 * max([len(line) for line in lines]) if lines else 7
            cell.comment.height = 20 * len(lines) if lines else 20

        # test datatype to overwrite datetime format
        if isinstance(value, datetime.date):
            cell.data_type = "d"
            cell.number_format = FORMAT_DATE_DDMMYY
        elif isinstance(value, (int, float)):
            cell.data_type = "n"
        else:
            cell.data_type = "s"

        return cell

    def make_row(self, values: list, style=None, comments=None) -> list:
        "build a row"
        # TODO make possible differents styles in a row
        if comments is None:
            comments = [None] * len(values)
        return [
            self.make_cell(value, style, comment)
            for value, comment in zip(values, comments)
        ]

    def append_single_cell_row(self, value: any, style=None, prefix=None):
        """construit une ligne composée d'une seule cellule et l'ajoute à la feuille.
        mêmes paramètres que make_cell:
        value -- contenu de la cellule (texte ou numérique)
        style -- style par défaut de la feuille si non spécifié
        prefix -- cellules ajoutées au début de la ligne
        """
        self.append_row((prefix or []) + [self.make_cell(value, style)])

    def append_blank_row(self):
        """construit une ligne vide et l'ajoute à la feuille."""
        self.append_row([None])

    def append_row(self, row):
        """ajoute une ligne déjà construite à la feuille."""
        self.rows.append(row)

    def prepare(self):
        """génére un flux décrivant la feuille.
        Ce flux pourra ensuite être repris dans send_excel_file (classeur mono feille)
        ou pour la génération d'un classeur multi-feuilles
        """
        for k, v in self.column_dimensions.items():
            self.ws.column_dimensions[k] = v

        for k, v in self.row_dimensions.items():
            self.ws.row_dimensions[k] = self.row_dimensions[v]
        for row in self.rows:
            self.ws.append(row)

    def generate(self, column_widths=None) -> AnyStr:
        """génération d'un classeur mono-feuille"""
        # this method makes sense for standalone worksheet (else call workbook.generate())
        if self.wb is None:  # embeded sheet
            raise ScoValueError("can't generate a single sheet from a ScoWorkbook")

        # construction d'un flux
        # https://openpyxl.readthedocs.io/en/stable/tutorial.html#saving-as-a-stream
        self.prepare()

        # largeur des colonnes
        if column_widths:
            for k, v in column_widths.items():
                self.set_column_dimension_width(k, v)

        if self.auto_filter is not None:
            self.ws.auto_filter.ref = self.auto_filter
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            return tmp.read()


def excel_simple_table(
    titles: list[str] = None,
    lines: list[list[str]] = None,
    sheet_name: str = "feuille",
    titles_styles=None,
    comments: list[str] | None = None,
):
    """Export simple type 'CSV': 1ere ligne en gras, le reste tel quel.
    comments (optionnel) donne des commentaires à placer sur les cellules de titre.
    """
    ws = ScoExcelSheet(sheet_name)
    if titles is None:
        titles = []
    if lines is None:
        lines = [[]]
    if titles_styles is None:
        style = excel_make_style(bold=True)
        titles_styles = [style] * len(titles)
    if comments is None:
        comments = [None] * len(titles)
    # ligne de titres
    ws.append_row(
        [
            ws.make_cell(it, style, comment)
            for (it, style, comment) in zip(titles, titles_styles, comments)
        ]
    )
    default_style = excel_make_style()
    text_style = excel_make_style(number_format=FORMAT_GENERAL)
    int_style = excel_make_style()
    float_style = excel_make_style(number_format=FORMAT_NUMBER_00)
    for line in lines:
        cells = []
        for it in line:
            cell_style = default_style
            if isinstance(it, float):
                cell_style = float_style
            elif isinstance(it, int):
                cell_style = int_style
            else:
                cell_style = text_style
            cells.append(ws.make_cell(it, cell_style))
        ws.append_row(cells)
    return ws.generate()


def excel_bytes_to_list(bytes_content) -> tuple[list, list[list]]:
    """Lecture d'un flux xlsx.
    returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of lists: the spreadsheet cells
    """
    try:
        filelike = io.BytesIO(bytes_content)
        return _excel_to_list(filelike)
    except Exception as exc:
        raise ScoValueError(
            """Le fichier xlsx attendu n'est pas lisible ! (1)
            Peut-être avez-vous fourni un fichier au mauvais format (txt, xls, ..)
            """
        ) from exc


def excel_bytes_to_dict(
    bytes_content, force_lowercase_keys=True
) -> tuple[list[str], list[dict]]:
    """Lecture d'un flux xlsx et conversion en dict,
    les clés étant données par les titres sur la première ligne.
    returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of dict: the spreadsheet cells
    """
    diag, rows = excel_bytes_to_list(bytes_content)
    if len(rows) < 1:
        raise ScoValueError("Fichier excel vide")
    if force_lowercase_keys:
        keys = [k.strip().lower() for k in rows[0]]
    else:
        keys = [k.strip() for k in rows[0]]
    return diag, [dict(zip(keys, row)) for row in rows[1:]]


def excel_file_to_list(filelike) -> tuple[list, list[list]]:
    """Lecture d'un flux xlsx
    returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of lists: the spreadsheet cells
    """
    try:
        return _excel_to_list(filelike)
    except Exception as exc:
        raise ScoValueError(
            """Le fichier xlsx attendu n'est pas lisible ! (2)
            Peut-être avez-vous fourni un fichier au mauvais format (txt, xls, ...)
            """
        ) from exc


def _open_workbook(filelike, dump_debug=False) -> Workbook:
    """Open document.
    On error, if dump-debug is True, dump data in /tmp for debugging purpose
    """
    try:
        workbook = load_workbook(filename=filelike, read_only=True, data_only=True)
    except Exception as exc:
        log("Excel_to_list: failure to import document")
        if dump_debug:
            dump_filename = "/tmp/last_scodoc_import_failure" + scu.XLSX_SUFFIX
            log(f"Dumping problematic file on {dump_filename}")
            with open(dump_filename, "wb") as f:
                f.write(filelike)
        raise ScoValueError(
            "Fichier illisible: assurez-vous qu'il s'agit bien d'un document Excel xlsx !"
        ) from exc
    return workbook


def _excel_to_list(filelike) -> tuple[list, list[list]]:
    """returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of lists: the spreadsheet cells
    """
    workbook = _open_workbook(filelike)
    diag = []  # liste de chaines pour former message d'erreur
    if len(workbook.sheetnames) < 1:
        diag.append("Aucune feuille trouvée dans le classeur !")
        return diag, None
    # n'utilise que la première feuille:
    if len(workbook.sheetnames) > 1:
        diag.append("Attention: n'utilise que la première feuille du classeur !")
    sheet_name = workbook.sheetnames[0]
    ws = workbook[sheet_name]
    diag_sheet, matrix = _excel_sheet_to_list(ws, sheet_name)
    diag += diag_sheet
    return diag, matrix


def _excel_sheet_to_list(sheet: Worksheet, sheet_name: str) -> tuple[list, list[list]]:
    """read a spreadsheet sheet, and returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of lists: the spreadsheet cells
    """
    diag = []
    # fill matrix
    values = {}
    for row in sheet.iter_rows():
        for cell in row:
            if cell.value is not None:
                values[(cell.row - 1, cell.column - 1)] = str(cell.value)
    if not values:
        diag.append(f"Aucune valeur trouvée dans la feuille {sheet_name} !")
        return diag, None
    indexes = list(values.keys())
    # search numbers of rows and cols
    rows = [x[0] for x in indexes]
    cols = [x[1] for x in indexes]
    nbcols = max(cols) + 1
    nbrows = max(rows) + 1
    matrix = []
    for _ in range(nbrows):
        matrix.append([""] * nbcols)

    for row_idx, col_idx in indexes:
        v = values[(row_idx, col_idx)]
        matrix[row_idx][col_idx] = v
    diag.append(f'Feuille "{sheet_name}", {len(matrix)} lignes')

    return diag, matrix


def excel_workbook_to_list(filelike):
    """Lit un classeur (workbook): chaque feuille est lue
        et est convertie en une liste de listes.
    Returns:
    - diag : a list of strings (error messages aimed at helping the user)
    - a list of lists: the spreadsheet cells
    """
    try:
        workbook = _open_workbook(filelike)
    except Exception as exc:
        raise ScoValueError(
            """Le fichier xlsx attendu n'est pas lisible ! (3)
            Peut-être avez-vous fourni un fichier au mauvais format (txt, xls, ...)
            """
        ) from exc
    diag = []  # liste de chaines pour former message d'erreur
    if len(workbook.sheetnames) < 1:
        diag.append("Aucune feuille trouvée dans le classeur !")
        return diag, None
    matrix_list = []
    for sheet_name in workbook.sheetnames:
        # fill matrix
        sheet = workbook.get_sheet_by_name(sheet_name)
        diag_sheet, matrix = _excel_sheet_to_list(sheet, sheet_name)
        diag += diag_sheet
        matrix_list.append(matrix)
    return diag, matrix_list


# TODO déplacer dans un autre fichier
def excel_feuille_listeappel(
    sem,
    groupname,
    lines,
    partitions=None,
    with_codes=False,
    with_date_inscription=False,
    with_paiement=False,
    server_name=None,
    edt_params: dict = None,
):
    """Génération feuille appel.

    edt_params :
        - "discipline" : Discipline
        - "ens" : Enseignant
        - "date" : Date (format JJ/MM/AAAA)
        - "heure" : Heure (format HH:MM)
    """
    # Obligatoire sinon import circulaire
    # pylint: disable=import-outside-toplevel
    from app.scodoc.sco_groups import listgroups_abbrev, get_etud_groups

    if edt_params is None:
        edt_params = {}

    if partitions is None:
        partitions = []
    formsemestre_id = sem["formsemestre_id"]
    sheet_name = "Liste " + groupname

    ws = ScoExcelSheet(sheet_name)
    ws.set_column_dimension_width("A", 3)
    max_name_width: int = 35
    letter_int: int = ord("B")

    font1 = Font(name="Arial", size=11)
    font1i = Font(name="Arial", size=10, italic=True)
    font1b = Font(name="Arial", size=11, bold=True)

    side_thin = Side(border_style="thin", color=COLORS.BLACK.value)

    border_tbl = Border(top=side_thin, bottom=side_thin, left=side_thin)
    border_tblr = Border(
        top=side_thin, bottom=side_thin, left=side_thin, right=side_thin
    )

    style1i = {
        "font": font1i,
    }

    style1b = {
        "font": font1,
        "border": border_tbl,
    }

    style2 = {
        "font": Font(name="Arial", size=14),
    }

    style2t3 = {
        "border": border_tblr,
    }

    style2t3bold = {
        "font": font1b,
        "border": border_tblr,
    }

    style3 = {
        "font": Font(name="Arial", bold=True, size=14),
    }

    # ligne 1
    title = "%s %s (%s - %s)" % (
        sco_preferences.get_preference("DeptName", formsemestre_id),
        notesdb.unquote(sem["titre_num"]),
        sem["date_debut"],
        sem["date_fin"],
    )

    ws.append_row([None, ws.make_cell(title, style2)])

    # ligne 2
    ws.append_row(
        [
            None,
            ws.make_cell("Discipline :", style2),
            ws.make_cell(edt_params.get("discipline", ""), style3),
        ]
    )

    # ligne 3
    cell_2 = ws.make_cell("Enseignant :", style2)
    ws.append_row([None, cell_2, ws.make_cell(edt_params.get("ens", ""), style3)])

    # ligne 4: Avertissement pour ne pas confondre avec listes notes + Date
    cell_1 = ws.make_cell("Date :", style2)
    cell_2 = ws.make_cell(
        "Ne pas utiliser cette feuille pour saisir les notes !", style1i
    )
    ws.append_row([None, cell_1, ws.make_cell(edt_params.get("date", ""))])

    # ligne 5 : Heure
    ws.append_row(
        [
            None,
            ws.make_cell("Heure :", style2),
            ws.make_cell(edt_params.get("heure", "")),
        ]
    )

    # ligne 6: groupe
    ws.append_row([None, ws.make_cell(f"Groupe {groupname}", style3)])

    ws.append_blank_row()
    ws.append_row([None, cell_2])

    # ligne 9: Entête (contruction dans une liste cells)
    cell_2 = ws.make_cell("Nom", style3)
    cells = [None, cell_2]
    letter_int += 1
    p_name: list = []
    for partition in partitions:
        p_name.append(partition["partition_name"])

    p_name: str = " / ".join(p_name)
    ws.set_column_dimension_width(chr(letter_int), len(p_name))
    if with_codes:
        cells.append(ws.make_cell("etudid", style3))
        cells.append(ws.make_cell("code_nip", style3))
        cells.append(ws.make_cell("code_ine", style3))
    if with_date_inscription:
        cells.append(ws.make_cell("Date inscr.", style3))
    # case Groupes
    cells.append(ws.make_cell("Groupes", style3))
    letter_int += 1
    ws.set_column_dimension_width(chr(letter_int), 30)

    # case émargement
    cells.append(ws.make_cell("Émargement", style3))
    letter_int += 1
    ws.set_column_dimension_width(chr(letter_int), 30)
    ws.append_row(cells)

    row_id: int = len(ws.rows) + 1
    n = 0
    # pour chaque étudiant
    for t in lines:
        n += 1
        nomprenom = (
            t["civilite_str"] + " " + t["nom"] + " " + t["prenom"].lower().capitalize()
        )
        name_width = min(max_name_width, (len(nomprenom) + 2.0) * 1.25)
        ws.set_column_dimension_width("B", name_width)
        style_nom = style2t3
        if with_paiement:
            paie = t.get("paiementinscription", None)
            if paie is None:
                nomprenom += " (inscription ?)"
                style_nom = style2t3bold
            elif not paie:
                nomprenom += " (non paiement)"
                style_nom = style2t3bold
        cell_1 = ws.make_cell(n, style1b)
        cell_2 = ws.make_cell(nomprenom, style_nom)
        cells = [cell_1, cell_2]
        group = get_etud_groups(t["etudid"], formsemestre_id=formsemestre_id)
        cells.append(ws.make_cell(listgroups_abbrev(group), style2t3))
        if with_codes:
            cells.append(ws.make_cell(t["etudid"], style2t3))
            code_nip = t.get("code_nip", "")
            cells.append(ws.make_cell(code_nip, style2t3))
            code_ine = t.get("code_ine", "")
            cells.append(ws.make_cell(code_ine, style2t3))
        if with_date_inscription:
            event = ScolarEvent.query.filter_by(
                etudid=t["etudid"],
                event_type="INSCRIPTION",
                formsemestre_id=formsemestre_id,
            ).first()
            if event:
                date_inscription = event.event_date
                cells.append(ws.make_cell(date_inscription, style2t3))
        cells.append(ws.make_cell(style=style2t3))
        ws.append_row(cells)
        ws.set_row_dimension_height(row_id, 30)
        row_id += 1

    ws.append_blank_row()

    # bas de page (date, serveur)
    dt = time.strftime(scu.DATEATIME_FMT)
    if server_name:
        dt += " sur " + server_name
    cell_2 = ws.make_cell(("Liste éditée le " + dt), style1i)
    ws.append_row([None, cell_2])

    return ws.generate()
