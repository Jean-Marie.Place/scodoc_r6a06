# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Exception handling
"""
from flask_login import current_user
import app


# --- Exceptions
class ScoException(Exception):
    "super classe de toutes les exceptions ScoDoc."


class InvalidNoteValue(ScoException):
    "Valeur note invalide. Usage interne saisie note."


class ScoInvalidCSRF(ScoException):
    "Erreur validation token CSRF"


class ScoValueError(ScoException):
    """Exception avec page d'erreur utilisateur
    - dest_url : url où aller après la page d'erreur
    - safe (default False): si vrai, affiche le message non html quoté.
    """

    # mal nommée: super classe de toutes les exceptions avec page
    # d'erreur gentille.
    def __init__(self, msg, dest_url=None, dest_label=None, safe=False):
        super().__init__(msg)
        # champs utilisés par template sco_value_error.j2
        self.dest_url = dest_url
        self.dest_label = dest_label
        self.safe = safe


class ScoPermissionDenied(ScoValueError):
    """Permission non accordée (appli web)"""

    def __init__(self, msg=None, dest_url=None, safe=False):
        if msg is None:
            msg = f"""Opération non autorisée pour {
                current_user.get_nomcomplet() if current_user else "?"
            }. Pas la permission, ou objet verrouillé."""
        super().__init__(msg, dest_url=dest_url, safe=safe)


class ScoBugCatcher(ScoException):
    "bug avec enquete en cours"


class NoteProcessError(ScoValueError):
    "Valeurs notes invalides"


class InvalidEtudId(NoteProcessError):
    pass


class ScoFormatError(ScoValueError):
    "Erreur lecture d'un fichier fourni par l'utilisateur"

    def __init__(self, msg, filename="", dest_url=None, safe=False):
        super().__init__(msg, dest_url=dest_url, safe=safe)
        self.filename = filename


class ScoInvalidParamError(ScoValueError):
    """Paramètres requete invalides.
    Utilisée lorsqu'une route est appelée avec des paramètres invalides
    (id strings, ...)
    """

    def __init__(self, msg=None, dest_url=None, safe=False):
        msg = msg or "Adresse invalide. Vérifiez vos signets."
        super().__init__(msg, dest_url=dest_url, safe=safe)


class ScoPDFFormatError(ScoValueError):
    "erreur génération PDF (templates platypus, ...)"

    def __init__(self, msg, dest_url=None, safe=False):
        super().__init__(
            f"""Erreur dans un format pdf:
            <p>{msg}</p>
            <p>Vérifiez les paramètres (polices de caractères, balisage, réglages bulletins...)
            dans les paramètres ou préférences.
            </p>
            """,
            dest_url=dest_url,
            safe=safe,
        )


class ScoInvalidDept(ScoValueError):
    """departement invalide"""

    pass


class ScoConfigurationError(ScoValueError):
    """Configuration invalid"""

    pass


class ScoLockedFormError(ScoValueError):
    "Modification d'une formation verrouillée"

    def __init__(self, msg="", dest_url=None, safe=False):
        msg = (
            "Cette formation est verrouillée (car il y a un semestre verrouillé qui s'y réfère). "
            + str(msg)
        )
        super().__init__(msg=msg, dest_url=dest_url, safe=safe)


class ScoLockedSemError(ScoValueError):
    "Modification d'un formsemestre verrouillé"

    def __init__(self, msg="", dest_url=None, safe=False):
        msg = "Ce semestre est verrouillé ! " + str(msg)
        super().__init__(msg=msg, dest_url=dest_url, safe=safe)


class ScoNonEmptyFormationObject(ScoValueError):
    """On ne peut pas supprimer un module/matiere ou UE si des formsemestre s'y réfèrent"""

    def __init__(self, type_objet="objet'", msg="", dest_url=None, safe=False):
        msg = f"""<h3>{type_objet} "{msg}" utilisé(e) dans des semestres: suppression impossible.</h3>
            <p class="help">Il faut d'abord supprimer le semestre (ou en retirer ce {type_objet}).
            Mais il est peut-être préférable de laisser ce programme intact et d'en créer une
            nouvelle version pour la modifier sans affecter les semestres déjà en place.
            </p>
            """
        super().__init__(msg=msg, dest_url=dest_url, safe=safe)


class ScoInvalidIdType(ScoValueError):
    """Pour les clients qui s'obstinent à utiliser des bookmarks
    ou historiques anciens avec des ID ScoDoc7.
    """

    def __init__(self, msg=""):
        import app.scodoc.sco_utils as scu

        msg = f"""<h3>Adresse de page invalide</h3>
            <p class="help">
            Vous utilisez un lien invalide, qui correspond probablement
            à une ancienne version du logiciel. <br>
            Au besoin, mettre à jour vos marque-pages.
            </p>
            <p> Si le problème persiste, merci de contacter l'assistance
            via la liste de diffusion <a href="{scu.SCO_USERS_LIST}">Notes</a>
            ou de préférence le <a href="{scu.SCO_DISCORD_ASSISTANCE}">salon Discord</a>.
            </p>
            <p>Message serveur: <tt>{msg}</tt></p>
        """
        super().__init__(msg)


class ScoNoReferentielCompetences(ScoValueError):
    """Formation APC (BUT) non associée à référentiel de compétences"""

    def __init__(self, msg: str = "", formation: "Formation" = None):
        formation_title = (
            f"{formation.titre} version {formation.version}" if formation else ""
        )
        msg = f"""
        <p>Pas de référentiel de compétences associé à la formation {formation_title}!
        </p>
        <p>Pour associer un référentiel, passer par le menu <b>Semestre /
        Voir la formation... </b> et suivre le lien <em>"associer à un référentiel
        de compétences"</em>
        """
        super().__init__(msg)


class ScoGenError(ScoValueError):
    "exception avec affichage d'une page explicative ad-hoc"

    def __init__(self, msg="", safe=False):
        super().__init__(msg, safe=safe)


class AccessDenied(ScoGenError):
    pass


class ScoInvalidDateError(ScoValueError):
    pass


class APIInvalidParams(Exception):
    """Exception pour les API JSON"""

    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        "dict"
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv


class ScoFormationConflict(Exception):
    """Conflit cohérence formation (APC)"""


class ScoTemporaryError(ScoValueError):
    """Erreurs temporaires rarissimes (caches ?)"""

    def __init__(self, msg: str = ""):
        msg = """
        <p>Erreur temporaire</p>
        <p>Veuillez ré-essayer. Si le problème persiste (ou s'il venait
        à se produire fréquemment), merci de contacter l'assistance ScoDoc
        (voir <a href="https://scodoc.org/Contact/">les informations de contact</a>).
        </p>
        """
        app.clear_scodoc_cache()
        super().__init__(msg)
