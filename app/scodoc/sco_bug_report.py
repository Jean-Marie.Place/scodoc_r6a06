# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Rapport de bug ScoDoc

Permet de créer un rapport de bug (ticket) sur la plateforme git scodoc.org.

Le principe est le suivant:
 1- Si l'utilisateur le demande, on dump la base de données et on l'envoie

 2- ScoDoc envoie une requête POST à scodoc.org pour qu'un ticket git soit créé avec les
    informations fournies par l'utilisateur + quelques métadonnées.

"""
from flask import g
from flask_login import current_user
import requests

import app.scodoc.sco_utils as scu
import sco_version

from app import log
from app.scodoc.sco_dump_db import sco_dump_and_send_db
from app.scodoc.sco_exceptions import ScoValueError


def sco_bug_report(
    title: str = "", message: str = "", etab: str = "", include_dump: bool = False
) -> requests.Response:
    """Envoi d'un bug report (ticket)"""
    dump_id = None

    if include_dump:
        dump = sco_dump_and_send_db()

        try:
            dump_id = dump.json()["dump_id"]
        except (requests.exceptions.JSONDecodeError, KeyError):
            dump_id = "inconnu (erreur)"

    log(f"sco_bug_report: {scu.SCO_BUG_REPORT_URL} by {current_user.user_name}")
    try:
        r = requests.post(
            scu.SCO_BUG_REPORT_URL,
            json={
                "ticket": {
                    "title": title,
                    "message": message,
                    "etab": etab,
                    "dept": getattr(g, "scodoc_dept", "-"),
                },
                "user": {
                    "name": current_user.get_nomcomplet(),
                    "email": current_user.email,
                },
                "dump": {
                    "included": include_dump,
                    "id": dump_id,
                },
                "scodoc": {
                    "version": sco_version.SCOVERSION,
                },
            },
            timeout=scu.SCO_ORG_TIMEOUT,
        )

    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as exc:
        log("ConnectionError: Impossible de joindre le serveur d'assistance")
        raise ScoValueError(
            """
        Impossible de joindre le serveur d'assistance (scodoc.org).
        Veuillez contacter le service informatique de votre établissement pour
        corriger la configuration de ScoDoc. Dans la plupart des cas, il
        s'agit d'un proxy mal configuré.
        """
        ) from exc

    return r
