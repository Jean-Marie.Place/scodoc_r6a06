# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Modalites des semestres

La "modalite" est utilisee pour organiser les listes de semestres sur la page d'accueil.

Elle n'est pas utilisée pour les parcours, ni pour rien d'autre
(c'est donc un attribut "cosmétique").

"""
import collections
from app.models import FormSemestre, FormationModalite


def _list_formsemestres_modalites(
    formsemestres: list[FormSemestre],
) -> list[FormationModalite]:
    """Liste ordonnée des modalités présentes dans ces formsemestres"""
    modalites = {}
    for formsemestre in formsemestres:
        if formsemestre.modalite not in modalites:
            m = FormationModalite.query.filter_by(
                modalite=formsemestre.modalite
            ).first()
            if m is not None:
                modalites[m.modalite] = m
    return sorted(modalites.values(), key=lambda m: m.numero)


def group_formsemestres_by_modalite(
    formsemestres: list[FormSemestre],
) -> dict[str, list[FormSemestre]]:
    """Given the list of formsemestre, group them by modalite,
    sorted in each one by semestre id and date
    """
    sems_by_mod = collections.defaultdict(list)
    modalites = _list_formsemestres_modalites(formsemestres)
    sems_by_mod = {
        modalite.modalite: [
            formsemestre
            for formsemestre in formsemestres
            if formsemestre.modalite == modalite.modalite
        ]
        for modalite in modalites
    }
    # tri dans chaque modalité par indice de semestre et date debut
    for modalite in modalites:
        sems_by_mod[modalite.modalite].sort(
            key=lambda x: (
                x.semestre_id if x.semestre_id > 0 else -1000 * x.semestre_id,
                x.date_debut,
            )
        )

    return sems_by_mod, modalites
