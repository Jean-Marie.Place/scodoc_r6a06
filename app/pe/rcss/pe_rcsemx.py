##############################################################################
#  Module "Avis de poursuite d'étude"
#  conçu et développé par Cléo Baras (IUT de Grenoble)
##############################################################################

"""
Created on 01-2024

@author: barasc
"""

from app.models import FormSemestre
from app.pe.moys import pe_sxtag
from app.pe.rcss import pe_rcs, pe_trajectoires


class RCSemX(pe_rcs.RCS):
    """Modélise un regroupement cohérent de SemX (en même regroupant
    des semestres Sx combinés pour former les résultats des étudiants
    au semestre de rang x) dans le but de synthétiser les résultats
    du S1 jusqu'au semestre final ciblé par le RCSemX (dépendant de l'aggrégat
    visé).

    Par ex: Si l'aggrégat du RCSemX est '3S' (=S1+S2+S3),
    regroupement le SemX du S1 + le SemX du S2 + le SemX du S3 (chacun
    incluant des infos sur les redoublements).

    Args:
        nom: Un nom du RCS (par ex: '5S')
        semestre_final: Le semestre final du RCS
    """

    def __init__(self, nom: str, semestre_final: FormSemestre):
        pe_rcs.RCS.__init__(self, nom, semestre_final)

        self.semXs_aggreges: dict[(str, int) : pe_sxtag.SxTag] = {}
        """Les semX à aggréger"""

    def add_semXs(self, semXs: dict[(str, int) : pe_trajectoires.SemX]):
        """Ajoute des semXs aux semXs à regrouper dans le RCSemX

        Args:
            semXs: Dictionnaire ``{(str,fid): RCF}`` à ajouter
        """
        self.semXs_aggreges = self.semXs_aggreges | semXs

    def get_repr(self, verbose=True) -> str:
        """Représentation textuelle d'un RCSF
        basé sur ses RCF aggrégés"""
        title = f"""{self.__class__.__name__} {pe_rcs.RCS.__str__(self)}"""
        if verbose:
            noms = []
            for semx_id, semx in self.semXs_aggreges.items():
                noms.append(semx.get_repr(verbose=False))
            if noms:
                title += " <<" + "+".join(noms) + ">>"
            else:
                title += " <<vide>>"
        return title
