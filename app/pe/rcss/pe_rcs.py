##############################################################################
#  Module "Avis de poursuite d'étude"
#  conçu et développé par Cléo Baras (IUT de Grenoble)
##############################################################################

"""
Created on 01-2024

@author: barasc
"""

from app.models import FormSemestre

TYPES_RCS = {
    "S1": {
        "aggregat": ["S1"],
        "descr": "Semestre 1 (S1)",
    },
    "S2": {
        "aggregat": ["S2"],
        "descr": "Semestre 2 (S2)",
    },
    "1A": {
        "aggregat": ["S1", "S2"],
        "descr": "BUT1 (S1+S2)",
    },
    "S3": {
        "aggregat": ["S3"],
        "descr": "Semestre 3 (S3)",
    },
    "S4": {
        "aggregat": ["S4"],
        "descr": "Semestre 4 (S4)",
    },
    "2A": {
        "aggregat": ["S3", "S4"],
        "descr": "BUT2 (S3+S4)",
    },
    "3S": {
        "aggregat": ["S1", "S2", "S3"],
        "descr": "Moyenne du S1 au S3 (S1+S2+S3)",
    },
    "4S": {
        "aggregat": ["S1", "S2", "S3", "S4"],
        "descr": "Moyenne du S1 au S4 (S1+S2+S3+S4)",
    },
    "S5": {
        "aggregat": ["S5"],
        "descr": "Semestre 5 (S5)",
    },
    "S6": {
        "aggregat": ["S6"],
        "descr": "Semestre 6 (S6)",
    },
    "3A": {
        "aggregat": ["S5", "S6"],
        "descr": "BUT3 (S5+S6)",
    },
    "5S": {
        "aggregat": ["S1", "S2", "S3", "S4", "S5"],
        "descr": "Moyenne du S1 au S5 (S1+S2+S3+S4+S5)",
    },
    "6S": {
        "aggregat": ["S1", "S2", "S3", "S4", "S5", "S6"],
        "descr": "Moyenne globale (S1+S2+S3+S4+S5+S6)",
    },
}
"""Dictionnaire détaillant les différents regroupements cohérents
de semestres (RCS), en leur attribuant un nom et en détaillant
le nom des semestres qu'ils regroupent et l'affichage qui en sera fait
dans les tableurs de synthèse.
"""

TOUS_LES_RCS_AVEC_PLUSIEURS_SEM = [cle for cle in TYPES_RCS if not cle.startswith("S")]
TOUS_LES_RCS = list(TYPES_RCS.keys())
TOUS_LES_SEMESTRES = [cle for cle in TYPES_RCS if cle.startswith("S")]


def get_descr_rcs(nom_rcs: str) -> str:
    """Renvoie la description pour les tableurs de synthèse
    Excel d'un nom de RCS"""
    return TYPES_RCS[nom_rcs]["descr"]


class RCS:
    """Modélise un regroupement cohérent de semestres,
    tous se terminant par un (form)semestre final.
    """

    def __init__(self, nom: str, semestre_final: FormSemestre):
        self.nom: str = nom
        """Nom du RCS"""
        assert self.nom in TOUS_LES_RCS, "Le nom d'un RCS doit être un aggrégat"

        self.aggregat: list[str] = TYPES_RCS[nom]["aggregat"]
        """Aggrégat (liste des nom des semestres aggrégés)"""

        self.formsemestre_final: FormSemestre = semestre_final
        """(Form)Semestre final du RCS"""

        self.rang_final = self.formsemestre_final.semestre_id
        """Rang du formsemestre final"""

        self.rcs_id: (str, int) = (nom, semestre_final.formsemestre_id)
        """Identifiant du RCS sous forme (nom_rcs, id du semestre_terminal)"""

        self.fid_final: int = self.formsemestre_final.formsemestre_id
        """Identifiant du (Form)Semestre final"""

    def get_formsemestre_id_final(self) -> int:
        """Renvoie l'identifiant du formsemestre final du RCS

        Returns:
            L'id du formsemestre final (marquant la fin) du RCS
        """
        return self.formsemestre_final.formsemestre_id

    def __str__(self):
        """Représentation textuelle d'un RCS"""
        return f"{self.nom}[#{self.formsemestre_final.formsemestre_id}✟{self.formsemestre_final.date_fin.year}]"

    def get_repr(self, verbose=True):
        """Représentation textuelle d'un RCS"""
        return self.__str__()

    def __eq__(self, other):
        """Egalité de RCS"""
        return (
            self.nom == other.nom
            and self.formsemestre_final == other.formsemestre_final
        )
