import numpy as np
import pandas as pd

from app.comp.moy_sem import comp_ranks_series
from app.pe import pe_affichage


class Moyenne:
    COLONNES = [
        "note",
        "classement",
        "rang",
        "min",
        "max",
        "moy",
        "nb_etuds",
        "nb_inscrits",
    ]
    """Colonnes du df"""

    @classmethod
    def get_colonnes_synthese(cls, with_min_max_moy):
        if with_min_max_moy:
            return ["note", "rang", "min", "max", "moy"]
        else:
            return ["note", "rang"]

    def __init__(self, notes: pd.Series):
        """Classe centralisant la synthèse des moyennes/classements d'une série
        de notes :

        * des "notes" : la Serie pandas des notes (float),
        * des "classements" : la Serie pandas des classements (float),
        * des "min" : la note minimum,
        * des "max" : la note maximum,
        * des "moy" : la moyenne,
        * des "nb_inscrits" : le nombre d'étudiants ayant une note,
        """
        self.notes = notes
        """Les notes"""
        self.etudids = list(notes.index)  # calcul à venir
        """Les id des étudiants"""
        self.inscrits_ids = notes[notes.notnull()].index.to_list()
        """Les id des étudiants dont la note est non nulle"""
        self.df: pd.DataFrame = self.comp_moy_et_stat(self.notes)
        """Le dataframe retraçant les moyennes/classements/statistiques"""
        self.synthese = self.to_dict()
        """La synthèse (dictionnaire) des notes/classements/statistiques"""

    def comp_moy_et_stat(self, notes: pd.Series) -> dict:
        """Calcule et structure les données nécessaires au PE pour une série
        de notes (pouvant être une moyenne d'un tag à une UE ou une moyenne générale
        d'un tag) dans un dictionnaire spécifique.

        Partant des notes, sont calculés les classements (en ne tenant compte
        que des notes non nulles).

        Args:
            notes: Une série de notes (avec des éventuels NaN)

        Returns:
            Un dictionnaire stockant les notes, les classements, le min,
            le max, la moyenne, le nb de notes (donc d'inscrits)
        """
        df = pd.DataFrame(
            np.nan,
            index=self.etudids,
            columns=Moyenne.COLONNES,
        )

        # Supprime d'éventuelles chaines de caractères dans les notes
        notes = pd.to_numeric(notes, errors="coerce")
        df["note"] = notes

        # Les nb d'étudiants & nb d'inscrits
        df["nb_etuds"] = len(self.etudids)
        df["nb_etuds"] = df["nb_etuds"].astype(int)

        # Les étudiants dont la note n'est pas nulle
        inscrits_ids = notes[notes.notnull()].index.to_list()
        df.loc[inscrits_ids, "nb_inscrits"] = len(inscrits_ids)
        # df["nb_inscrits"] = df["nb_inscrits"].astype(int)

        # Le classement des inscrits
        notes_non_nulles = notes[inscrits_ids]
        (class_str, class_int) = comp_ranks_series(notes_non_nulles)
        df.loc[inscrits_ids, "classement"] = class_int
        # df["classement"] = df["classement"].astype(int)

        # Le rang (classement/nb_inscrit)
        df["rang"] = df["rang"].astype(str)
        df.loc[inscrits_ids, "rang"] = (
            df.loc[inscrits_ids, "classement"].astype(int).astype(str)
            + "/"
            + df.loc[inscrits_ids, "nb_inscrits"].astype(int).astype(str)
        )

        # Les stat (des inscrits)
        df.loc[inscrits_ids, "min"] = notes.min()
        df.loc[inscrits_ids, "max"] = notes.max()
        df.loc[inscrits_ids, "moy"] = notes.mean()

        return df

    def get_df_synthese(self, with_min_max_moy=None):
        """Renvoie le df de synthese limité aux colonnes de synthese"""
        colonnes_synthese = Moyenne.get_colonnes_synthese(
            with_min_max_moy=with_min_max_moy
        )
        df = self.df[colonnes_synthese].copy()
        df["rang"] = df["rang"].replace("nan", "")
        return df

    def to_dict(self) -> dict:
        """Renvoie un dictionnaire de synthèse des moyennes/classements/statistiques générale (but)"""
        synthese = {
            "notes": self.df["note"],
            "classements": self.df["classement"],
            "min": self.df["min"].mean(),
            "max": self.df["max"].mean(),
            "moy": self.df["moy"].mean(),
            "nb_inscrits": self.df["nb_inscrits"].mean(),
        }
        return synthese

    def is_significatif(self) -> bool:
        """Indique si la moyenne est significative (c'est-à-dire à des notes)"""
        return self.synthese["nb_inscrits"] > 0
