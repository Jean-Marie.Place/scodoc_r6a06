##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Description d'un formsemestre pour applications tierces.

Ces informations sont éditables dans ScoDoc et publiés sur l'API
pour affichage dans l'application tierce.
"""

from app import db
from app import models


class FormSemestreDescription(models.ScoDocModel):
    """Informations décrivant un "semestre" (session) de formation
    pour un apprenant.
    """

    __tablename__ = "notes_formsemestre_description"

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text(), nullable=False, default="", server_default="")
    "description du cours, html autorisé"
    horaire = db.Column(db.Text(), nullable=False, default="", server_default="")
    "indication sur l'horaire, texte libre"
    date_debut_inscriptions = db.Column(db.DateTime(timezone=True), nullable=True)
    date_fin_inscriptions = db.Column(db.DateTime(timezone=True), nullable=True)

    wip = db.Column(db.Boolean, nullable=False, default=False, server_default="false")
    "work in progress: si vrai, affichera juste le titre du semestre"

    # Store image data directly in the database:
    image = db.Column(db.LargeBinary(), nullable=True)
    campus = db.Column(db.Text(), nullable=False, default="", server_default="")
    salle = db.Column(db.Text(), nullable=False, default="", server_default="")

    dispositif = db.Column(db.Integer, nullable=False, default=0, server_default="0")
    "0 présentiel, 1 online, 2 hybride"
    dispositif_descr = db.Column(
        db.Text(), nullable=False, default="", server_default=""
    )
    "décrit modalités de formation. html autorisé."
    modalites_mcc = db.Column(db.Text(), nullable=False, default="", server_default="")
    "modalités de contrôle des connaissances (texte libre, html autorisé)"
    photo_ens = db.Column(db.LargeBinary(), nullable=True)
    "photo de l'enseignant(e)"
    public = db.Column(db.Text(), nullable=False, default="", server_default="")
    "public visé"
    prerequis = db.Column(db.Text(), nullable=False, default="", server_default="")
    "prérequis (texte libre, html autorisé)"
    responsable = db.Column(db.Text(), nullable=False, default="", server_default="")
    "responsable du cours (texte libre, html autorisé)"
    formsemestre_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_formsemestre.id", ondelete="CASCADE"),
        nullable=False,
    )
    formsemestre = db.relationship(
        "FormSemestre", back_populates="description", uselist=False
    )

    def __repr__(self):
        return f"<FormSemestreDescription {self.id} {self.formsemestre}>"

    def clone(self, not_copying=()) -> "FormSemestreDescription":
        """clone instance"""
        return super().clone(not_copying=not_copying + ("formsemestre_id",))

    def to_dict(self, exclude_images=True) -> dict:
        "dict, tous les attributs sauf les images"
        d = dict(self.__dict__)
        d.pop("_sa_instance_state", None)
        if exclude_images:
            d.pop("image", None)
            d.pop("photo_ens", None)
        return d


FORMSEMESTRE_DISPOSITIFS = {
    0: "présentiel",
    1: "en ligne",
    2: "hybride",
}
