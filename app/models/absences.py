# -*- coding: UTF-8 -*

"""Gestion des absences
"""

from app import db, models


class Absence(models.ScoDocModel):
    """LEGACY
    Ce modèle n'est PLUS UTILISE depuis ScoDoc 9.6 et remplacé par assiduité.
    une absence (sur une demi-journée)
    """

    __tablename__ = "absences"
    id = db.Column(db.Integer, primary_key=True)
    etudid = db.Column(
        db.Integer, db.ForeignKey("identite.id", ondelete="CASCADE"), index=True
    )
    jour = db.Column(db.Date)
    estabs = db.Column(db.Boolean())
    estjust = db.Column(db.Boolean())
    matin = db.Column(db.Boolean())
    # motif de l'absence:
    description = db.Column(db.Text())
    entry_date = db.Column(db.DateTime(timezone=True), server_default=db.func.now())
    # moduleimpid concerne (optionnel):
    moduleimpl_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_moduleimpl.id", ondelete="SET NULL"),
    )

    def to_dict(self):
        data = {
            "id": self.id,
            "etudid": self.etudid,
            "jour": self.jour,
            "estabs": self.estabs,
            "estjust": self.estjust,
            "matin": self.matin,
            "description": self.description,
            "entry_date": self.entry_date,
            "moduleimpl_id": self.moduleimpl_id,
        }
        return data


class AbsenceNotification(models.ScoDocModel):
    """Notification d'absence émise"""

    __tablename__ = "absences_notifications"

    id = db.Column(db.Integer, primary_key=True)
    etudid = db.Column(
        db.Integer,
        db.ForeignKey("identite.id", ondelete="CASCADE"),
    )
    notification_date = db.Column(
        db.DateTime(timezone=True), server_default=db.func.now()
    )
    email = db.Column(db.Text())
    nbabs = db.Column(db.Integer)
    nbabsjust = db.Column(db.Integer)
    formsemestre_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_formsemestre.id", ondelete="CASCADE"),
    )


class BilletAbsence(models.ScoDocModel):
    """Billet d'absence (signalement par l'étudiant)"""

    __tablename__ = "billet_absence"

    id = db.Column(db.Integer, primary_key=True)
    etudid = db.Column(
        db.Integer,
        db.ForeignKey("identite.id"),
        index=True,
    )
    abs_begin = db.Column(db.DateTime(timezone=True))
    abs_end = db.Column(db.DateTime(timezone=True))
    # raison de l'absence:
    description = db.Column(db.Text())
    # False: new, True: processed
    etat = db.Column(db.Boolean(), default=False, server_default="false")
    entry_date = db.Column(db.DateTime(timezone=True), server_default=db.func.now())
    # true si l'absence _pourrait_ etre justifiée
    justified = db.Column(db.Boolean(), default=False, server_default="false")

    def to_dict(self):
        return {
            "id": self.id,
            "billet_id": self.id,
            "etudid": self.etudid,
            "abs_begin": self.abs_begin,
            "abs_end": self.abs_begin,
            "description": self.description,
            "etat": self.etat,
            "entry_date": self.entry_date,
            "justified": self.justified,
        }
