"""
Nouvelles vues pour les groupes
(aux normes ScoDoc9)
"""

from flask import render_template, request

from app import log
from app.decorators import (
    scodoc,
    permission_required,
)
from app.forms.scolar import groups_form
from app.models import FormSemestre
from app.scodoc.sco_excel import excel_feuille_listeappel
from app.scodoc.sco_exceptions import ScoValueError
from app.scodoc.sco_groups_view import DisplayedGroupsInfos, menu_groups_choice
from app.scodoc.sco_permissions import Permission
from app.scodoc import sco_utils as scu
from app.views import ScoData
from app.views import scolar_bp as bp


@bp.route("/formulaire_feuille_appel/<int:formsemestre_id>", methods=["GET", "POST"])
@scodoc
@permission_required(Permission.ScoView)
def formulaire_feuille_appel(formsemestre_id: int):
    """Formulaire de feuille d'appel

    GET Affiche le formulaire de remplissage de la feuille d'appel
    POST (soumission) retourne la feuille d'appel correspondante

    QUERY
    -----
    group_ids:<list:<int:group_id>>

    """
    formsemestre = FormSemestre.get_formsemestre(formsemestre_id)

    if request.method == "GET":
        group_ids: list[int] = request.args.getlist("group_ids")
        data = {"group_ids": group_ids}
    else:
        group_ids_list = request.form.getlist("group_ids")
        try:
            group_ids = [int(gid) for gid in group_ids_list if gid]
        except ValueError as exc:
            log(f"formulaire_feuille_appel: group_ids invalide: {group_ids_list[:100]}")
            raise ScoValueError("groupes invalides") from exc
        data = {}

    form = groups_form.FeuilleAppelPreForm(request.form, data=data)

    groups_infos = DisplayedGroupsInfos(
        group_ids,
        formsemestre_id=formsemestre_id,
        select_all_when_unspecified=True,
    )

    if form.validate_on_submit():
        edt_params: dict = {
            "date": form.date.data or "",
            "heure": form.heure.data or "",
            "discipline": form.discipline.data or "",
            "ens": form.ens.data or "",
        }

        xls = excel_feuille_listeappel(
            groups_infos.formsemestre,
            groups_infos.groups_titles,
            groups_infos.members,
            partitions=groups_infos.partitions,
            edt_params=edt_params,
        )

        filename = f"liste_{groups_infos.groups_filename}"
        return scu.send_file(xls, filename, scu.XLSX_SUFFIX, scu.XLSX_MIMETYPE)

    return render_template(
        "scolar/formulaire_feuille_appel.j2",
        form=form,
        group_name=groups_infos.groups_titles,
        grp=menu_groups_choice(groups_infos),
        formsemestre_id=formsemestre_id,
        sco=ScoData(formsemestre=formsemestre),
    )
