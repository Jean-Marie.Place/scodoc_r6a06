// Formulaire saisie des notes

document.addEventListener("DOMContentLoaded", function () {
  let noteInputs = document.querySelectorAll("#formnotes .note");
  noteInputs.forEach(function (input) {
    input.addEventListener("input", function() {
      this.setAttribute("data-modified", "true");
    });
    input.addEventListener("blur", input.addEventListener("blur", function(event){write_on_blur(event.currentTarget)} ));
  });

  var formInputs = document.querySelectorAll("#formnotes input");
  formInputs.forEach(function (input) {
    input.addEventListener("paste", paste_text);
  });

  var masquerBtn = document.querySelector(".btn_masquer_DEM");
  masquerBtn.addEventListener("click", masquer_DEM);
});

function write_on_blur(elt) {
    if (elt.getAttribute("data-modified") === "true" && elt.value !== elt.getAttribute("data-last-saved-value")) {
      valid_note.call(elt);
      elt.setAttribute("data-modified", "false"); // Reset the modified flag
    }
}

function is_valid_note(v) {
  if (!v) return true;

  var note_min = parseFloat(document.querySelector("#eval_note_min").textContent);
  var note_max = parseFloat(document.querySelector("#eval_note_max").textContent);

  if (!v.match("^-?[0-9]*.?[0-9]*$")) {
    return v == "ABS" || v == "EXC" || v == "SUPR" || v == "ATT" || v == "DEM";
  } else {
    var x = parseFloat(v);
    return x >= note_min && x <= note_max;
  }
}

function valid_note(e) {
  var v = this.value.trim().toUpperCase().replace(",", ".");
  if (is_valid_note(v)) {
    if (v && v != this.getAttribute("data-last-saved-value")) {
      this.className = "note_valid_new";
      const etudid = parseInt(this.getAttribute("data-etudid"));
      save_note(this, v, etudid);
    }
  } else {
    /* Saisie invalide */
    this.className = "note_invalid";
    sco_message("valeur invalide ou hors barème");
  }
}

let isSaving = false; // true si requête en cours

async function save_note(elem, v, etudid) {
  let evaluation_id = document.querySelector("#formnotes_evaluation_id").getAttribute("value");
  let formsemestre_id = document.querySelector("#formnotes_formsemestre_id").getAttribute("value");
  var scoMsg = document.getElementById("sco_msg");
  scoMsg.innerHTML = "en cours...";
  scoMsg.style.display = "block";
  isSaving = true; // Set the flag to true when the request starts
  try {
    const response = await fetch(
      SCO_URL + "../api/evaluation/" + evaluation_id + "/notes/set",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          notes: [[etudid, v]],
          comment: document.getElementById("formnotes_comment").value,
        }),
      }
    );
    if (!response.ok) {
      sco_message("Erreur: valeur non enregistrée");
    } else {
      const data = await response.json();
      var scoMsg = document.getElementById("sco_msg");
      scoMsg.style.display = "none";
      if (data.etudids_changed.length > 0) {
        sco_message("enregistré");
        elem.className = "note_saved";
        // Il y avait une decision de jury ?
        if (data.etudids_with_decision.includes(etudid)) {
          if (v !== elem.getAttribute("data-orig-value")) {
            var juryLink = document.getElementById("jurylink_" + etudid);
            juryLink.innerHTML = `<a href="${SCO_URL}Notes/formsemestre_validation_etud_form?formsemestre_id=${formsemestre_id}&etudid=${etudid}"
              >mettre à jour décision de jury</a>`;
          } else {
            var juryLink = document.getElementById("jurylink_" + etudid);
            juryLink.innerHTML = "";
          }
        }
        // Mise à jour menu historique
        if (data.history_menu[etudid]) {
          var historyElem = document.getElementById("hist_" + etudid);
          historyElem.innerHTML = data.history_menu[etudid];
        }
        elem.setAttribute("data-last-saved-value", v);
      }
    }
  } catch (error) {
    console.error("Fetch error:", error);
    sco_message("Erreur réseau: valeur non enregistrée");
  } finally {
    isSaving = false; // Reset the flag when the request is complete
  }
}

// Set up the beforeunload event listener
window.addEventListener('beforeunload', function (e) {
  let noteInputs = document.querySelectorAll("#formnotes .note");
  noteInputs.forEach(function (input) {
    if (input.getAttribute("data-modified") === "true" && input.value !== input.getAttribute("data-last-saved-value")) {
      valid_note.call(input);
    }
  });
  if (isSaving) {
    // Display a confirmation dialog
    const confirmationMessage = 'Des modifications sont en cours de sauvegarde. Êtes-vous sûr de vouloir quitter cette page ?';
    e.preventDefault(); // Standard for most modern browsers
    e.returnValue = confirmationMessage; // For compatibility with older browsers
    return confirmationMessage; // For compatibility with older browsers
  }
});

function change_history(e) {
  let opt = e.selectedOptions[0];
  let val = opt.getAttribute("data-note");
  const etudid = parseInt(e.getAttribute("data-etudid"));
  // le input associé a ce menu:
  let input_elem = e.parentElement.parentElement.parentElement.childNodes[0];
  input_elem.value = val;
  save_note(input_elem, val, etudid);
}

// Contribution S.L.: copier/coller des notes

function paste_text(event) {
  event.stopPropagation();
  event.preventDefault();
  var clipb = event.clipboardData;
  var data = clipb.getData("Text");
  var list = data.split(/\r\n|\r|\n|\t| /g);
  var currentInput = event.currentTarget;
  var masquerDEM = document
    .querySelector("body")
    .classList.contains("masquer_DEM");

  for (var i = 0; i < list.length; i++) {
    if (!currentInput.disabled) { // skip DEM
       currentInput.value = list[i];
    }
    // --- trigger blur
    currentInput.setAttribute("data-modified", "true");
    var evt = new Event("blur", { bubbles: true, cancelable: true});
    currentInput.dispatchEvent(evt);
    // --- next input
    var sibbling = currentInput.parentElement.parentElement.nextElementSibling;
    while (
      sibbling &&
      (sibbling.style.display == "none" ||
        (masquerDEM && sibbling.classList.contains("etud_dem")))
    ) {
      sibbling = sibbling.nextElementSibling;
    }
    if (sibbling) {
      currentInput = sibbling.querySelector("input");
      if (!currentInput) {
        return;
      }
    } else {
      return;
    }
  }
}

function masquer_DEM() {
  document.querySelector("body").classList.toggle("masquer_DEM");
}
