/* Page accueil département */
var apo_ue_editor = null;
var apo_mod_editor = null;

$(document).ready(function () {
  var table_options = {
    paging: false,
    searching: false,
    info: false,
    /* "autoWidth" : false, */
    fixedHeader: {
      header: true,
      footer: true,
    },
    orderCellsTop: true, // cellules ligne 1 pour tri
    aaSorting: [], // Prevent initial sorting
  };
  $("table#formation_table_recap").DataTable(table_options);
  let table_editable = document.querySelector(
    "table#formation_table_recap.apo_editable"
  );
  if (table_editable) {
    let apo_ue_save_url = document.querySelector(
      "table#formation_table_recap.apo_editable"
    ).dataset.apo_ue_save_url;
    apo_ue_editor = new ScoFieldEditor(
      "table#formation_table_recap tr.ue td.apo",
      apo_ue_save_url,
      false
    );
    let apo_mod_save_url = document.querySelector(
      "table#formation_table_recap.apo_editable"
    ).dataset.apo_mod_save_url;
    apo_mod_editor = new ScoFieldEditor(
      "table#formation_table_recap tr.mod td.apo",
      apo_mod_save_url,
      false
    );
  }
});
