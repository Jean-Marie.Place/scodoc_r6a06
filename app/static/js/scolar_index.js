/* Page accueil département */
var apo_editor = null;
var elt_annee_apo_editor = null;
var elt_sem_apo_editor = null;

$(document).ready(function () {
  var table_options = {
    paging: false,
    searching: false,
    info: false,
    /* "autoWidth" : false, */
    fixedHeader: {
      header: true,
      footer: true,
    },
    orderCellsTop: true, // cellules ligne 1 pour tri
    aaSorting: [], // Prevent initial sorting
  };
  const table = new DataTable("table.semlist", table_options);
  // Sélection de semestres et mise à jour du menu associé
  if (document.getElementById("formsemestres-select-infos")) {
    table.on('click', 'tbody tr', function (e) {
      e.currentTarget.classList.toggle('selected');
      var nbSelectedRows = table.rows('.selected').count();
      if (nbSelectedRows == 0) {
        document.getElementById("formsemestres-select-infos").style.display = 'none';
      }
      else {
        document.getElementById("formsemestres-select-infos").style.display = 'inline';
        if (nbSelectedRows > 1) {
          document.querySelector("#formsemestres-select-menu li.sco_menu_title a").childNodes[1].nodeValue = nbSelectedRows + " semestres sélectionnés";
        } else {
          document.querySelector("#formsemestres-select-menu li.sco_menu_title a").childNodes[1].nodeValue = nbSelectedRows + " semestre sélectionné";
        }
      }
    });
    // Lien déselectionner
    document.getElementById("formsemestres-deselect").addEventListener('click', function (e) {
      e.preventDefault();
      table.rows('.selected').nodes().to$().removeClass('selected');
      document.getElementById("formsemestres-select-infos").style.display = 'none';
    });
    // Modification des liens de la section formsemestres-actions: ajout des formsemestres selectionnés:
    const links = document.querySelectorAll('#formsemestres-select-menu li.sco_menu_item a');
    links.forEach(link => {
      link.addEventListener('click', function(event) {
        // Prevent the default action (navigation)
        event.preventDefault();

        // Build the query string with formsemestre_id parameters
        const selectedRows = document.querySelectorAll('tr.selected');
        const selectedFormsemestreIds = Array.from(selectedRows).map(row => row.dataset.formsemestre_id);
        const queryString = selectedFormsemestreIds
          .map(id => `formsemestre_ids=${encodeURIComponent(id)}`)
          .join('&');

        // Construct the new URL
        const originalHref = link.getAttribute('href');
        const newHref = originalHref.includes('?')
          ? `${originalHref}&${queryString}` // If there's already a query string
          : `${originalHref}?${queryString}`; // If no query string exists

        // Navigate to the new URL
        window.location.href = newHref;
      });
    });
  }

  // Edition des codes Apo
  let table_editable = document.querySelector("table#semlist.apo_editable");
  if (table_editable) {
    let save_url = document.querySelector("table#semlist.apo_editable").dataset
      .apo_save_url;
    apo_editor = new ScoFieldEditor(".etapes_apo_str", save_url, false);

    save_url = document.querySelector("table#semlist.apo_editable").dataset
      .elt_annee_apo_save_url;
    elt_annee_apo_editor = new ScoFieldEditor(
      ".elt_annee_apo",
      save_url,
      false
    );

    save_url = document.querySelector("table#semlist.apo_editable").dataset
      .elt_sem_apo_save_url;
    elt_sem_apo_editor = new ScoFieldEditor(".elt_sem_apo", save_url, false);

    save_url = document.querySelector("table#semlist.apo_editable").dataset
      .elt_passage_apo_save_url;
    elt_passage_apo_editor = new ScoFieldEditor(".elt_passage_apo", save_url, false);
  }
});
