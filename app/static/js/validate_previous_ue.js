// Affiche et met a jour la liste des UE partageant le meme code

document.addEventListener("DOMContentLoaded", () => {
  update_ue_list();
  $("#tf_ue_id").bind("change", update_ue_list);

  const buttons = document.querySelectorAll(".ue_list_etud_validations button");

  buttons.forEach((button) => {
    button.addEventListener("click", (event) => {
      // Handle button click event here
      event.preventDefault();
      const etudid = event.target.dataset.etudid;
      const validation_id = event.target.dataset.v_id;
      const validation_type = event.target.dataset.type;
      if (confirm("Supprimer cette validation ?")) {
        delete_validation(etudid, validation_type, validation_id);
      }
    });
  });
});

async function delete_validation(etudid, validation_type, validation_id) {
  const response = await fetch(
    `${SCO_URL}../api/etudiant/${etudid}/jury/${validation_type}/${validation_id}/delete`,
    {
      method: "POST",
    }
  );
  if (response.ok) {
    location.reload();
  } else {
    const data = await response.json();
    sco_error_message("erreur: " + data.message);
  }
}

function update_ue_list() {
  var ue_id = $("#tf_ue_id")[0].value;
  if (ue_id) {
    var query = SCO_URL + "Notes/ue_sharing_code?ue_id=" + ue_id;
    $.get(query, "", function (data) {
      $("#ue_list_code").html(data);
    });
  }
}
