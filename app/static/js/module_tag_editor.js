// Edition tags sur modules

$(function () {
  $(".module_tag_editor").tagEditor({
    initialTags: "",
    placeholder: "Tags du module ...",
    forceLowercase: false,
    onChange: function (field, editor, tags) {
      $.post("module_tag_set", {
        module_id: field.data("module_id"),
        taglist: tags.join(),
      }).fail(function(jqXHR, textStatus, errorThrown) {
        alert("erreur: tag non enregistré");
      });
    },
    autocomplete: {
      delay: 200, // ms before suggest
      position: { collision: "flip" }, // automatic menu position up/down
      source: "module_tag_search",
    },
  });

  // version readonly
  readOnlyTags($(".module_tag_editor_ro"));

  // $(".sco_tag_checkbox").click(function () {
  //   if ($(this).is(":checked")) {
  //     $(".sco_tag_edit").show();
  //   } else {
  //     $(".sco_tag_edit").hide();
  //   }
  // });
});

// tags
function toggleEditDisplay(checkbox) {
  const isChecked = checkbox.checked;
  document.querySelectorAll('.sco_tag_edit').forEach(el => {
    el.style.display = isChecked ? 'block' : 'none';
  });
  // form semection de semestres:
  const showTagsInput = document.querySelector('input[name="show_tags"]');
  if (showTagsInput) {
    showTagsInput.value = isChecked ? '1' : '0';
  }
}

document.addEventListener('DOMContentLoaded', function() {
  document.querySelectorAll('.sco_tag_checkbox').forEach(checkbox => {
    // Set up initial state for each checkbox
    toggleEditDisplay(checkbox);

    // Add click event listener to each checkbox
    checkbox.addEventListener('click', function() {
      toggleEditDisplay(this);
    });
  });
});