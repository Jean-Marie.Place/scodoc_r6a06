// Affichage progressif du trombinoscope html

$().ready(function () {
  var spans = $(".unloaded_img");
  for (var i = 0; i < spans.length; i++) {
    var sp = spans[i];
    var etudid = sp.id;
    $(sp).load(SCO_URL + "etud_photo_html?etudid=" + etudid);
  }
});

// L'URL pour recharger l'état courant de la page (groupes et tab selectionnes)
// (ne fonctionne que pour les requetes GET: manipule la query string)

function groups_view_url() {
  let url = new URL(location.href);
  let urlParams = url.searchParams;
  // ajout du formsemestre
  urlParams.set(
    "formsemestre_id",
    $("#group_selector")[0].formsemestre_id.value
  );
  urlParams.delete("group_ids");
  // ajout des groupes selectionnes
  var selected_groups = document.getElementById("group_ids_sel").value;
  if (Array.isArray(selected_groups)) {
    selected_groups.forEach((value) => {
      urlParams.append("group_ids", value);
    });
  } else {
    urlParams.set("group_ids", selected_groups);
  }
  url.search = urlParams.toString();
  return url.href;
}

// Recharge la page sans arguments group_ids
function remove_group_filter() {
  var url = new URL(location.href);
  url.searchParams.delete("group_ids");

  window.location = url.href;
}

// Recharge la page en changeant les groupes selectionnés et en conservant le tab actif:
function submit_group_selector() {
  window.location = groups_view_url();
}

function change_list_options(selected_options) {
  var url = new URL(groups_view_url());
  var urlParams = url.searchParams;
  var options = [
    "with_paiement",
    "with_archives",
    "with_annotations",
    "with_codes",
    "with_date_inscription",
    "with_bourse",
  ];
  for (var i = 0; i < options.length; i++) {
    let option = options[i];
    urlParams.set(option, selected_options.indexOf(option) >= 0 ? "1" : "0");
  }
  window.location = url.href;
}

// Menu choix groupe:
function toggle_visible_etuds() {
  //
  document.querySelectorAll(".etud_elem").forEach((element) => {
    element.style.display = "none";
  });
  var qargs = "";
  var selectedOptions = document.querySelectorAll(
    "#group_ids_sel option:checked"
  );
  var qargs = "";
  selectedOptions.forEach(function (opt) {
    var group_id = opt.value;
    var groupElements = document.querySelectorAll(".group-" + group_id);
    groupElements.forEach(function (elem) {
      elem.style.display = "block";
    });
    qargs += "&group_ids=" + group_id;
  });
  // Update url saisie tableur:
  let input_eval = document.querySelectorAll("#formnotes_evaluation_id");
  if (input_eval.length > 0) {
    let evaluation_id = input_eval[0].value;
    let menu_saisie_tableur_a = document.querySelector(
      "#menu_saisie_tableur a"
    );
    menu_saisie_tableur_a.setAttribute(
      "href",
      "saisie_notes_tableur?evaluation_id=" + evaluation_id + qargs
    );
    // lien feuille excel:
    let lnk_feuille_saisie = document.querySelector("#lnk_feuille_saisie");
    lnk_feuille_saisie.setAttribute(
      "href",
      "feuille_saisie_notes?evaluation_id=" + evaluation_id + qargs
    );
  }
  // Update champs form group_ids_str
  let group_ids_str = Array.from(
    document.querySelectorAll("#group_ids_sel option:checked")
  )
    .map(function (elem) {
      return elem.value;
    })
    .join();
  document
    .querySelectorAll("input.group_ids_str")
    .forEach((elem) => (elem.value = group_ids_str));
}

// Trombinoscope
$().ready(function () {
  var elems = $(".trombi-photo");
  for (var i = 0; i < elems.length; i++) {
    $(elems[i]).qtip({
      content: {
        ajax: {
          url:
            SCO_URL +
            "etud_info_html?with_photo=0&etudid=" +
            get_etudid_from_elem(elems[i]),
        },
        text: "Loading...",
      },
      position: {
        at: "right",
        my: "left top",
      },
      style: {
        classes: "qtip-etud",
      },
      // utile pour debugguer le css:
      // hide: { event: 'unfocus' }
    });
  }
});

// gestion du tab actif
$().ready(function () {
  let tab = new URL(location.href).searchParams.get("tab");
  if (tab) {
    document.getElementById(tab)?.click();
  }
});
