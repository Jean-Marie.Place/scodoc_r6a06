##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""
  ScoDoc 9 API : accès aux moduleimpl

  CATEGORY
  --------
  ModuleImpl
"""

from flask_json import as_json
from flask_login import login_required

import app
from app import db
from app.api import api_bp as bp, api_web_bp
from app.api import api_permission_required as permission_required
from app.decorators import scodoc
from app.models import Identite, ModuleImpl, ModuleImplInscription
from app.scodoc import sco_cache, sco_liste_notes
from app.scodoc.sco_moduleimpl import do_moduleimpl_inscrit_etuds
from app.scodoc.sco_permissions import Permission
from app.scodoc.sco_utils import json_error


@bp.route("/moduleimpl/<int:moduleimpl_id>")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl(moduleimpl_id: int):
    """
    Retourne le moduleimpl.

    PARAMS
    ------
    moduleimpl_id : l'id d'un moduleimpl

    SAMPLES
    -------
    /moduleimpl/1
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    return modimpl.to_dict(convert_objects=True)


@bp.route("/moduleimpl/<int:moduleimpl_id>/inscriptions")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>/inscriptions")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl_inscriptions(moduleimpl_id: int):
    """Liste des inscriptions à ce moduleimpl.

    SAMPLES
    -------
    /moduleimpl/1/inscriptions
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    return [i.to_dict() for i in modimpl.inscriptions]


@bp.post("/moduleimpl/<int:moduleimpl_id>/etudid/<int:etudid>/inscrit")
@api_web_bp.post("/moduleimpl/<int:moduleimpl_id>/etudid/<int:etudid>/inscrit")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl_etud_inscrit(moduleimpl_id: int, etudid: int):
    """Inscrit l'étudiant à ce moduleimpl.

    SAMPLES
    -------
    /moduleimpl/1/etudid/2/inscrit
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    if not modimpl.can_change_inscriptions():
        return json_error(403, "opération non autorisée")
    etud = Identite.get_etud(etudid)
    do_moduleimpl_inscrit_etuds(modimpl.id, modimpl.formsemestre_id, [etud.id])
    app.log(f"moduleimpl_etud_inscrit: {etud} inscrit à {modimpl}")
    return (
        ModuleImplInscription.query.filter_by(moduleimpl_id=modimpl.id, etudid=etud.id)
        .first()
        .to_dict()
    )


@bp.post("/moduleimpl/<int:moduleimpl_id>/etudid/<int:etudid>/desinscrit")
@api_web_bp.post("/moduleimpl/<int:moduleimpl_id>/etudid/<int:etudid>/desinscrit")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl_etud_desinscrit(moduleimpl_id: int, etudid: int):
    """Désinscrit l'étudiant de ce moduleimpl.

    SAMPLES
    -------
    /moduleimpl/1/etudid/2/desinscrit
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    if not modimpl.can_change_inscriptions():
        return json_error(403, "opération non autorisée")
    etud = Identite.get_etud(etudid)
    inscription = ModuleImplInscription.query.filter_by(
        etudid=etud.id, moduleimpl_id=modimpl.id
    ).first()
    if inscription:
        db.session.delete(inscription)
        db.session.commit()
        sco_cache.invalidate_formsemestre(formsemestre_id=modimpl.formsemestre_id)
        app.log(f"moduleimpl_etud_desinscrit: {etud} inscrit à {modimpl}")
    return {"status": "ok"}


@bp.route("/moduleimpl/<int:moduleimpl_id>/notes")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>/notes")
@login_required
@scodoc
@permission_required(Permission.ScoView)
def moduleimpl_notes(moduleimpl_id: int):
    """Liste des notes dans ce moduleimpl.

    SAMPLES
    -------
    /moduleimpl/1/notes
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    app.set_sco_dept(modimpl.formsemestre.departement.acronym)
    table, _ = sco_liste_notes.do_evaluation_listenotes(
        moduleimpl_id=modimpl.id, fmt="json"
    )
    return table
