# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""API: gestion des logos
Contrib @jmp
"""

from datetime import datetime
from flask import Response, send_file
from flask_json import as_json

from app.api import api_bp as bp
from app.api import api_permission_required as permission_required
from app.decorators import scodoc
from app.models import Departement
from app.scodoc.sco_logos import list_logos, find_logo
from app.scodoc.sco_permissions import Permission
from app.scodoc.sco_utils import json_error


# Note: l'API logos n'est accessible qu'en mode global (avec jeton, sans dept)


@bp.route("/logos")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
@as_json
def logo_list_globals():
    """Liste des noms des logos définis pour le site ScoDoc.

    SAMPLES
    -------
    /logos
    """
    logos = list_logos()[None]
    return list(logos.keys())


@bp.route("/logo/<string:logoname>")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
def logo_get_global(logoname):
    """Renvoie le logo global de nom donné.

    L'image est au format png ou jpg; le format retourné dépend du format sous lequel
    l'image a été initialement enregistrée.

    SAMPLES
    -------
    /logo/B
    """
    logo = find_logo(logoname=logoname)
    if logo is None:
        return json_error(404, message="logo not found")
    logo.select()
    return send_file(
        logo.filepath,
        mimetype=f"image/{logo.suffix}",
        last_modified=datetime.now(),
    )


def _core_get_logos(dept_id) -> list:
    logos = list_logos().get(dept_id, dict())
    return list(logos.keys())


@bp.route("/departement/<string:dept_acronym>/logos")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
@as_json
def departement_logos(dept_acronym: str):
    """Liste des noms des logos définis pour le département
    désigné par son acronyme.

    SAMPLES
    -------
    /departement/TAPI/logos
    """
    dept_id = Departement.from_acronym(dept_acronym).id
    return _core_get_logos(dept_id)


@bp.route("/departement/id/<int:dept_id>/logos")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
@as_json
def departement_logos_by_id(dept_id):
    """Liste des noms des logos définis pour le département
    désigné par son id.
    """
    return _core_get_logos(dept_id)


def _core_get_logo(dept_id, logoname) -> Response:
    logo = find_logo(logoname=logoname, dept_id=dept_id)
    if logo is None:
        return json_error(404, message="logo not found")
    logo.select()
    return send_file(
        logo.filepath,
        mimetype=f"image/{logo.suffix}",
        last_modified=datetime.now(),
    )


@bp.route("/departement/<string:departement>/logo/<string:logoname>")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
def logo_get_local_dept_by_acronym(departement, logoname):
    """Le logo: image (format png ou jpg).

    **Exemple d'utilisation:**

        * `/ScoDoc/api/departement/MMI/logo/header`
    """
    dept_id = Departement.from_acronym(departement).id
    return _core_get_logo(dept_id, logoname)


@bp.route("/departement/id/<int:dept_id>/logo/<string:logoname>")
@scodoc
@permission_required(Permission.ScoSuperAdmin)
def logo_get_local_dept_by_id(dept_id, logoname):
    """Le logo: image (format png ou jpg).

    **Exemple d'utilisation:**

            * `/ScoDoc/api/departement/id/3/logo/header`
    """
    return _core_get_logo(dept_id, logoname)
