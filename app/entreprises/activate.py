##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Activation du module entreprises

L'affichage du module est contrôlé par la config ScoDocConfig.enable_entreprises

Au moment de l'activation, il est en général utile de proposer de configurer les
permissions de rôles standards: AdminEntreprise UtilisateurEntreprise ObservateurEntreprise

Voir associations dans sco_roles_default

"""
from app.auth.models import Role
from app.models import ScoDocSiteConfig
from app.scodoc.sco_roles_default import SCO_ROLES_ENTREPRISES_DEFAULT


def activate_module(
    enable: bool = True, set_default_roles_permission: bool = False
) -> bool:
    """Active le module et en option donne les permissions aux rôles standards.
    True si l'état d'activation a changé.
    """
    change = ScoDocSiteConfig.enable_entreprises(enable)
    if enable and set_default_roles_permission:
        Role.reset_roles_permissions(SCO_ROLES_ENTREPRISES_DEFAULT)
    return change
