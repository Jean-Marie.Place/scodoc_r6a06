##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Jury BUT et classiques: récupération des résults pour API
"""

import numpy as np

from app.but import jury_but
from app.models.etudiants import Identite
from app.models.formsemestre import FormSemestre
from app.scodoc import sco_pv_dict


def get_jury_but_results(formsemestre: FormSemestre) -> list[dict]:
    """Liste des résultats jury BUT ou classique sous forme de dict, pour API"""
    if (
        formsemestre.formation.is_apc()
        and formsemestre.formation.referentiel_competence is None
    ):
        # pas de ref. comp., donc pas de decisions de jury (ne lance pas d'exception)
        return []
    dpv = sco_pv_dict.dict_pvjury(formsemestre.id)
    rows = []
    for etudid in formsemestre.etuds_inscriptions:
        rows.append(_get_jury_but_etud_result(formsemestre, dpv, etudid))
    return rows


def _get_jury_but_etud_result(
    formsemestre: FormSemestre, dpv: dict, etudid: int
) -> dict:
    """Résultats de jury d'un étudiant sur un semestre, sous forme de dict"""
    etud = Identite.get_etud(etudid)
    dec_etud = dpv["decisions_dict"][etudid]
    if formsemestre.formation.is_apc():
        deca = jury_but.DecisionsProposeesAnnee(etud, formsemestre)
    else:
        deca = None
    row = {
        "etudid": etud.id,
        "code_nip": etud.code_nip,
        "code_ine": etud.code_ine,
        "is_apc": dpv["is_apc"],  # BUT ou classic ?
        "etat": dec_etud["etat"],  # I ou D ou DEF
        "nb_competences": deca.nb_competences if deca else 0,
    }
    # --- Les RCUEs
    rcue_list = []
    if deca:
        for dec_rcue in deca.get_decisions_rcues_annee():
            rcue = dec_rcue.rcue
            if rcue.complete:  # n'exporte que les RCUEs complets
                dec_ue1 = deca.decisions_ues[rcue.ue_1.id]
                dec_ue2 = deca.decisions_ues[rcue.ue_2.id]
                rcue_dict = {
                    "ue_1": {
                        "ue_id": rcue.ue_1.id,
                        "moy": (
                            None
                            if (dec_ue1.moy_ue is None or np.isnan(dec_ue1.moy_ue))
                            else dec_ue1.moy_ue
                        ),
                        "code": dec_ue1.code_valide,
                    },
                    "ue_2": {
                        "ue_id": rcue.ue_2.id,
                        "moy": (
                            None
                            if (dec_ue2.moy_ue is None or np.isnan(dec_ue2.moy_ue))
                            else dec_ue2.moy_ue
                        ),
                        "code": dec_ue2.code_valide,
                    },
                    "moy": rcue.moy_rcue,
                    "code": dec_rcue.code_valide,
                }
                rcue_list.append(rcue_dict)
    row["rcues"] = rcue_list
    # --- Les UEs
    ue_list = []
    if dec_etud["decisions_ue"]:
        for ue_id, ue_dec in dec_etud["decisions_ue"].items():
            ue_dict = {
                "ue_id": ue_id,
                "code": ue_dec["code"],
                "ects": ue_dec["ects"],
            }
            ue_list.append(ue_dict)
    row["ues"] = ue_list
    # --- Le semestre (pour les formations classiques)
    if dec_etud["decision_sem"]:
        row["semestre"] = {"code": dec_etud["decision_sem"].get("code")}
    else:
        row["semestre"] = {}  # APC, ...
    # --- L'année (BUT)
    if deca and deca.validation:
        row["annee"] = {
            "code": deca.validation.code,
            "ordre": deca.validation.ordre,
            "annee_scolaire": deca.validation.annee_scolaire,
        }
    else:
        row["annee"] = {}
    # --- Autorisations
    row["autorisations"] = dec_etud["autorisations"]
    return row
