##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""ScoDoc 9 : Formulaires / référentiel de compétence
"""

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import SelectField, SubmitField
from wtforms.validators import DataRequired


class FormationRefCompForm(FlaskForm):
    "Choix d'un référentiel"
    referentiel_competence = SelectField(
        "Choisir parmi les référentiels déjà chargés :"
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler")


class RefCompLoadForm(FlaskForm):
    "Upload d'un référentiel"
    referentiel_standard = SelectField(
        "Choisir un référentiel de compétences officiel BUT"
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler")

    def validate(self, extra_validators=None) -> bool:
        if not super().validate(extra_validators):
            return False
        if self.referentiel_standard.data == "0":
            self.referentiel_standard.errors.append("Choisir soit un référentiel")
            return False
        return True


class RefCompUploadForm(FlaskForm):
    "Upload d'un référentiel"
    upload = FileField(
        label="Sélectionner un fichier XML au format Orébut",
        validators=[
            FileAllowed(
                [
                    "xml",
                ],
                "Fichier XML Orébut seulement",
            ),
        ],
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler")

    def validate(self, extra_validators=None) -> bool:
        if not super().validate(extra_validators):
            return False
        if not self.upload.data:
            self.upload.errors.append("Choisir un fichier XML")
            return False
        return True


class FormationChangeRefCompForm(FlaskForm):
    "choix d'un nouveau ref. comp. pour une formation"
    object_select = SelectField(
        "Choisir le nouveau référentiel", validators=[DataRequired()]
    )
    submit = SubmitField("Changer le référentiel de la formation")
    cancel = SubmitField("Annuler")
