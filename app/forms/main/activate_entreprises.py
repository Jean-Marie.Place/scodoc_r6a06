"""
Formulaire activation module entreprises
"""

from flask_wtf import FlaskForm
from wtforms.fields.simple import BooleanField, SubmitField

from app.models import ScoDocSiteConfig


class ActivateEntreprisesForm(FlaskForm):
    "Formulaire activation module entreprises"
    set_default_roles_permission = BooleanField(
        "(re)mettre les rôles 'Entreprise' à leurs valeurs par défaut"
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
