# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire création de ticket de bug
"""

from flask_wtf import FlaskForm
from wtforms import SubmitField, validators
from wtforms.fields.simple import StringField, TextAreaField, BooleanField
from app.scodoc import sco_preferences


class CreateBugReport(FlaskForm):
    """Formulaire permettant la création d'un ticket de bug"""

    title = StringField(
        label="Titre du ticket",
        validators=[
            validators.DataRequired("titre du ticket requis"),
        ],
    )
    message = TextAreaField(
        label="Message",
        id="ticket_message",
        validators=[
            validators.DataRequired("message du ticket requis"),
        ],
    )
    etab = StringField(label="Etablissement")
    include_dump = BooleanField(
        """Inclure une copie anonymisée de la base de données ?
        Ces données faciliteront le traitement du problème et resteront strictement confidentielles.
        """,
        default=False,
    )
    submit = SubmitField("Envoyer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})

    def __init__(self, *args, **kwargs):
        super(CreateBugReport, self).__init__(*args, **kwargs)
        self.etab.data = sco_preferences.get_preference("InstituteName") or ""
