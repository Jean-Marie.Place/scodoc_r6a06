# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire configuration Module Assiduités
"""
import re

from flask_wtf import FlaskForm
from wtforms import DecimalField, SubmitField, ValidationError
from wtforms.fields.simple import StringField
from wtforms.validators import Optional, Length

from app.forms import ScoDateField, ScoTimeField


def check_tick_time(form, field):
    """Le tick_time doit être entre 0 et 60 minutes"""
    if field.data < 1 or field.data > 59:
        raise ValidationError("Valeur de granularité invalide (entre 1 et 59)")


def check_ics_path(form, field):
    """Vérifie que le chemin est bien un chemin absolu
    et qu'il contient edt_id
    """
    data = field.data.strip()
    if not data:
        return
    if not data.startswith("/"):
        raise ValidationError("Le chemin vers les ics doit commencer par /")
    if not "{edt_id}" in data:
        raise ValidationError("Le chemin vers les ics doit utiliser {edt_id}")


def check_ics_field(form, field):
    """Vérifie que c'est un nom de champ crédible: un mot alphanumérique"""
    if not re.match(r"^[a-zA-Z\-_0-9]+$", field.data):
        raise ValidationError("nom de champ ics invalide")


def check_ics_regexp(form, field):
    """Vérifie que field est une expresssion régulière"""
    value = field.data.strip()
    # check that it compiles
    try:
        _ = re.compile(value)
    except re.error as exc:
        raise ValidationError("expression invalide") from exc
    return True


class ConfigAssiduitesForm(FlaskForm):
    "Formulaire paramétrage Module Assiduité"
    assi_morning_time = ScoTimeField("Début de la journée", "assi_morning_time")
    assi_lunch_time = ScoTimeField(
        "Heure de midi (date pivot entre matin et après-midi)", "assi_lunch_time"
    )
    assi_afternoon_time = ScoTimeField("Fin de la journée", "assi_afternoon_time")

    assi_tick_time = DecimalField(
        "Granularité de la timeline (temps en minutes)",
        places=0,
        validators=[check_tick_time],
    )

    edt_ics_path = StringField(
        label="Chemin vers les ics",
        description="""Chemin absolu unix sur le serveur vers le fichier ics donnant l'emploi
        du temps d'un semestre. La balise <tt>{edt_id}</tt> sera remplacée par l'edt_id du
        semestre (par défaut, son code étape Apogée).
        Si ce champ n'est pas renseigné, les emplois du temps ne seront pas utilisés.""",
        validators=[Optional(), check_ics_path],
    )
    edt_ics_user_path = StringField(
        label="Chemin vers les ics des utilisateurs (enseignants)",
        description="""Optionnel. Chemin absolu unix sur le serveur vers le fichier ics donnant l'emploi
        du temps d'un enseignant. La balise <tt>{edt_id}</tt> sera remplacée par l'edt_id du
        de l'utilisateur.
        Dans certains cas (XXX), ScoDoc peut générer ces fichiers et les écrira suivant
        ce chemin (avec edt_id).
        """,
        validators=[Optional(), check_ics_path],
    )

    edt_ics_title_field = StringField(
        label="Champ contenant le titre",
        description="""champ de l'évènement calendrier: DESCRIPTION, SUMMARY, ...""",
        validators=[Optional(), check_ics_field],
    )
    edt_ics_title_regexp = StringField(
        label="Extraction du titre",
        description=r"""expression régulière python dont le premier groupe
            sera le titre de l'évènement affiché dans le calendrier ScoDoc.
            Exemple: <tt>Matière : \w+ - ([\w\.\s']+)</tt>
            """,
        validators=[Optional(), check_ics_regexp],
    )
    edt_ics_group_field = StringField(
        label="Champ contenant le groupe",
        description="""champ de l'évènement calendrier: DESCRIPTION, SUMMARY, ...""",
        validators=[Optional(), check_ics_field],
    )
    edt_ics_group_regexp = StringField(
        label="Extraction du groupe",
        description=r"""expression régulière python dont le premier groupe doit
            correspondre à l'identifiant de groupe de l'emploi du temps.
            Exemple: <tt>.*- ([\w\s]+)$</tt>
            """,
        validators=[Optional(), check_ics_regexp],
    )
    edt_ics_mod_field = StringField(
        label="Champ contenant le module",
        description="""champ de l'évènement calendrier: DESCRIPTION, SUMMARY, ...""",
        validators=[Optional(), check_ics_field],
    )
    edt_ics_mod_regexp = StringField(
        label="Extraction du module",
        description=r"""expression régulière python dont le premier groupe doit
            correspondre à l'identifiant (code) du module de l'emploi du temps.
            Exemple: <tt>Matière : ([A-Z][A-Z0-9]+)</tt>
            """,
        validators=[Optional(), check_ics_regexp],
    )
    edt_ics_uid_field = StringField(
        label="Champ contenant les enseignants",
        description="""champ de l'évènement calendrier: DESCRIPTION, SUMMARY, ...""",
        validators=[Optional(), check_ics_field],
    )
    edt_ics_uid_regexp = StringField(
        label="Extraction des enseignants",
        description=r"""expression régulière python permettant d'extraire les
            identifiants des enseignants associés à l'évènement.
            (contrairement aux autres champs, il peut y avoir plusieurs enseignants par évènement.)
            Exemple: <tt>[0-9]+</tt>
            """,
        validators=[Optional(), check_ics_regexp],
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
