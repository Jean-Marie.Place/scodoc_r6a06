##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire options génération table poursuite études (PE)
"""

from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, SubmitField


class ParametrageClasseurPE(FlaskForm):
    "Formulaire paramétrage génération classeur PE"
    # cohorte_restreinte = BooleanField(
    #    "Restreindre aux étudiants inscrits dans le semestre (sans interclassement de promotion) (à venir)"
    # )
    moyennes_tags = BooleanField(
        "Générer les moyennes sur les tags de modules personnalisés (cf. programme de formation)",
        default=True,
        render_kw={"checked": ""},
    )
    moyennes_ue_res_sae = BooleanField(
        "Générer les moyennes des ressources et des SAEs",
        default=True,
        render_kw={"checked": ""},
    )
    moyennes_ues_rcues = BooleanField(
        "Générer les moyennes par RCUEs (compétences) et leurs synthèses HTML étudiant par étudiant",
        default=True,
        render_kw={"checked": ""},
    )

    min_max_moy = BooleanField("Afficher les colonnes min/max/moy")

    # synthese_individuelle_etud = BooleanField(
    #    "Générer (suppose les RCUES)"
    # )
    publipostage = BooleanField(
        "Nomme les moyennes pour publipostage",
        # default=False,
        # render_kw={"checked": ""},
    )
    submit = SubmitField("Générer les classeurs poursuites d'études")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
