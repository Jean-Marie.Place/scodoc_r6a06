# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire ajout d'une "assiduité" sur un étudiant
Formulaire ajout d'un justificatif sur un étudiant
"""

from flask_wtf import FlaskForm
from flask_wtf.file import MultipleFileField
from wtforms import (
    BooleanField,
    SelectField,
    SubmitField,
    RadioField,
    TextAreaField,
    validators,
)
from wtforms.validators import DataRequired
from app.forms import ScoDateField, ScoTimeField


class AjoutAssiOrJustForm(FlaskForm):
    """Elements communs aux deux formulaires ajout
    assiduité et justificatif
    """

    def __init__(self, *args, **kwargs):
        "Init form, adding a filed for our error messages"
        super().__init__(*args, **kwargs)
        self.ok = True
        self.error_messages: list[str] = []  # used to report our errors

    def set_error(self, err_msg, field=None):
        "Set error message both in form and field"
        self.ok = False
        self.error_messages.append(err_msg)
        if field:
            field.errors.append(err_msg)

    def disable_all(self):
        "Disable all fields"
        for field in self:
            field.render_kw = {"disabled": True}

    date_debut = ScoDateField("Date de début", "assi_date_debut")
    heure_debut = ScoTimeField("Heure début", "assi_heure_debut")
    heure_fin = ScoTimeField("Heure fin", "assi_heure_fin")

    date_fin = ScoDateField("Date de fin (si plusieurs jours)", "assi_date_fin")

    entry_date = ScoDateField("Date de dépôt ou saisie", "entry_date")
    entry_time = ScoTimeField("Heure dépôt", "entry_time")

    submit = SubmitField("Enregistrer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})


class AjoutAssiduiteEtudForm(AjoutAssiOrJustForm):
    "Formulaire de saisie d'une assiduité pour un étudiant"

    description = TextAreaField(
        "Description",
        render_kw={
            "id": "description",
            "cols": 75,
            "rows": 4,
            "maxlength": 500,
        },
    )
    assi_etat = RadioField(
        "Signaler:",
        choices=[("absent", "absence"), ("retard", "retard"), ("present", "présence")],
        default="absent",
        validators=[
            validators.DataRequired("spécifiez le type d'évènement à signaler"),
        ],
    )
    modimpl = SelectField(
        "Module",
        choices={},  # will be populated dynamically
    )
    est_just = BooleanField("Justifiée")


class AjoutJustificatifEtudForm(AjoutAssiOrJustForm):
    "Formulaire de saisie d'un justificatif pour un étudiant"

    raison = TextAreaField(
        "Raison",
        render_kw={
            "id": "raison",
            "cols": 75,
            "rows": 4,
            "maxlength": 500,
        },
    )
    etat = SelectField(
        "État du justificatif",
        choices=[],  # sera rempli dynamiquement
        validators=[DataRequired(message="This field is required.")],
    )
    fichiers = MultipleFileField(label="Ajouter des fichiers")
