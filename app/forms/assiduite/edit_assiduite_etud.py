""" """

from flask_wtf import FlaskForm
from wtforms import SelectField, RadioField, TextAreaField, validators, SubmitField

from app.forms import ScoDateField, ScoTimeField
from app.scodoc.sco_utils import EtatAssiduite


class EditAssiForm(FlaskForm):
    """
    Formulaire de modification d'une assiduité
    """

    def __init__(self, *args, **kwargs):
        "Init form, adding a filed for our error messages"
        super().__init__(*args, **kwargs)
        self.ok = True
        self.error_messages: list[str] = []  # used to report our errors

    def set_error(self, err_msg, field=None):
        "Set error message both in form and field"
        self.ok = False
        self.error_messages.append(err_msg)
        if field:
            field.errors.append(err_msg)

    def disable_all(self):
        "Disable all fields"
        for field in self:
            field.render_kw = {"disabled": True}

    assi_etat = RadioField(
        "État:",
        choices=[
            (EtatAssiduite.ABSENT.value, EtatAssiduite.ABSENT.version_lisible()),
            (EtatAssiduite.RETARD.value, EtatAssiduite.RETARD.version_lisible()),
            (EtatAssiduite.PRESENT.value, EtatAssiduite.PRESENT.version_lisible()),
        ],
        default="absent",
        validators=[
            validators.DataRequired("spécifiez le type d'évènement à signaler"),
        ],
    )
    modimpl = SelectField(
        "Module",
        choices={},  # will be populated dynamically
    )
    description = TextAreaField(
        "Description",
        render_kw={
            "id": "description",
            "cols": 75,
            "rows": 4,
            "maxlength": 500,
        },
    )
    date_debut = ScoDateField("Date de début", "assi_date_debut")
    heure_debut = ScoTimeField("Heure début", "assi_heure_debut")
    heure_fin = ScoTimeField("Heure fin", "assi_heure_fin")

    date_fin = ScoDateField("Date de fin", "assi_date_fin")
    entry_date = ScoDateField("Date de dépôt ou saisie", "entry_date")
    entry_time = ScoTimeField("Heure dépôt", "entry_time")

    submit = SubmitField("Enregistrer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
