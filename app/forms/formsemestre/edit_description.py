##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Formulaire édition description formsemestre
"""
from wtforms import (
    BooleanField,
    FileField,
    SelectField,
    StringField,
    TextAreaField,
    SubmitField,
)
from flask_wtf.file import FileAllowed
from wtforms.validators import AnyOf, Optional, DataRequired

from app.forms import ScoDocForm
from app.formsemestre.import_from_descr import describe_field
from app.models import FORMSEMESTRE_DISPOSITIFS
from app.scodoc import sco_utils as scu


class DateDMYField(StringField):
    "Champ date JJ/MM/AAAA"

    def __init__(self, *args, **kwargs):
        render_kw = kwargs.pop("render_kw", {})
        render_kw.update({"class": "datepicker", "size": 10})
        super().__init__(*args, render_kw=render_kw, **kwargs)

    # note: process_formdata(self, valuelist) ne fonctionne pas
    # en cas d'erreur de saisie les valeurs ne sont pas ré-affichées.
    # On vérifie donc les valeurs dans le code de la vue.

    def process_data(self, value):
        "Process data from model to form"
        if value:
            self.data = value.strftime(scu.DATE_FMT)
        else:
            self.data = ""


class FormSemestreDescriptionForm(ScoDocForm):
    "Formulaire édition description formsemestre"
    description = TextAreaField(
        "Description",
        validators=[Optional()],
        description=describe_field("descr_description"),
    )
    horaire = StringField(
        "Horaire", validators=[Optional()], description=describe_field("descr_horaire")
    )
    date_debut_inscriptions = DateDMYField(
        "Date de début des inscriptions",
        description=describe_field("descr_date_debut_inscriptions"),
        render_kw={
            "id": "date_debut_inscriptions",
        },
    )
    date_fin_inscriptions = DateDMYField(
        "Date de fin des inscriptions",
        description=describe_field("descr_date_fin_inscriptions"),
        render_kw={
            "id": "date_fin_inscriptions",
        },
    )
    image = FileField(
        "Image", validators=[Optional()], description=describe_field("descr_image")
    )
    campus = StringField(
        "Campus", validators=[Optional()], description=describe_field("descr_campus")
    )
    salle = StringField(
        "Salle", validators=[Optional()], description=describe_field("descr_salle")
    )
    dispositif = SelectField(
        "Dispositif",
        choices=FORMSEMESTRE_DISPOSITIFS.items(),
        coerce=int,
        description=describe_field("descr_dispositif"),
        validators=[AnyOf(FORMSEMESTRE_DISPOSITIFS.keys())],
    )
    dispositif_descr = TextAreaField(
        "Description du dispositif",
        validators=[Optional()],
        description=describe_field("descr_dispositif_descr"),
    )
    modalites_mcc = TextAreaField(
        "Modalités de contrôle des connaissances",
        validators=[Optional()],
        description=describe_field("descr_modalites_mcc"),
    )
    photo_ens = FileField(
        "Photo de l'enseignant(e)",
        validators=[Optional()],
        description="ou autre illustration",
    )
    public = StringField(
        "Public visé", validators=[Optional()], description="ex: débutants"
    )
    prerequis = TextAreaField(
        "Prérequis", validators=[Optional()], description="texte libre. HTML autorisé."
    )
    responsable = StringField(
        "Responsable",
        validators=[Optional()],
        description=describe_field("descr_responsable"),
    )

    wip = BooleanField(
        "Travaux en cours",
        description="work in progress: si coché, affichera juste le titre du semestre",
    )

    submit = SubmitField("Enregistrer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})


class FormSemestresImportFromDescrForm(ScoDocForm):
    """Formulaire import excel semestres"""

    fichier = FileField(
        "Fichier à importer",
        validators=[
            DataRequired(),
            FileAllowed(["xlsx"], "Fichier .xlsx uniquement"),
        ],
    )
    image_archive_file = FileField(
        "Fichier zip avec les images",
        validators=[
            FileAllowed(["zip"], "Fichier .zip uniquement"),
        ],
    )
    create_formation = BooleanField(
        "Créer les programmes de formations s'ils n'existent pas", default=True
    )
    submit = SubmitField("Importer et créer les formations")
    cancel = SubmitField("Annuler")
