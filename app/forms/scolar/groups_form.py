"""
Formulaire FlaskWTF pour les groupes
"""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators


class FeuilleAppelPreForm(FlaskForm):
    """
    Formulaire utiliser dans le téléchargement des feuilles d'émargement
    """

    def __init__(self, *args, **kwargs):
        "Init form, adding a filed for our error messages"
        super().__init__(*args, **kwargs)
        self.ok = True
        self.error_messages: list[str] = []

    def set_error(self, err_msg, field=None):
        "Set error message both in form and field"
        self.ok = False
        self.error_messages.append(err_msg)
        if field:
            field.errors.append(err_msg)

    discipline = StringField(
        "Discipline",
    )

    ens = StringField(
        "Enseignant",
    )

    date = StringField(
        "Date de la séance",
        validators=[validators.Length(max=10)],
        render_kw={
            "class": "datepicker",
            "size": 10,
            "id": "date",
        },
    )

    heure = StringField(
        "Heure de début de la séance",
        default="",
        validators=[validators.Length(max=5)],
        render_kw={
            "class": "timepicker",
            "size": 5,
            "id": "heure",
        },
    )

    submit = SubmitField(
        "Télécharger la liste d'émargement", id="btn-submit", name="btn-submit"
    )
