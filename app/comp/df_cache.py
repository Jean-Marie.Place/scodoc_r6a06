# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""caches pour tables APC
"""
from flask import g

from app.scodoc import sco_cache


class ModuleCoefsCache(sco_cache.ScoDocCache):
    """Cache for module coefs
    Clé: formation_id.semestre_idx
    Valeur: DataFrame (df_load_module_coefs)
    """

    prefix = "MCO"


class EvaluationsPoidsCache(sco_cache.ScoDocCache):
    """Cache for poids evals
    Clé: moduleimpl_id
    Valeur: DataFrame (load_evaluations_poids)
    """

    prefix = "EPC"

    @classmethod
    def invalidate_all(cls):
        "delete all cached evaluations poids (in current dept)"
        from app.models.formsemestre import FormSemestre
        from app.models.moduleimpls import ModuleImpl

        moduleimpl_ids = [
            mi.id
            for mi in ModuleImpl.query.join(FormSemestre).filter_by(
                dept_id=g.scodoc_dept_id
            )
        ]
        cls.delete_many(moduleimpl_ids)

    @classmethod
    def invalidate_sem(cls, formsemestre_id):
        "delete cached evaluations poids for this formsemestre from cache"
        from app.models.moduleimpls import ModuleImpl

        moduleimpl_ids = [
            mi.id for mi in ModuleImpl.query.filter_by(formsemestre_id=formsemestre_id)
        ]
        cls.delete_many(moduleimpl_ids)
