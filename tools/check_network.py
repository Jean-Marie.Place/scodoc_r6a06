# test simple de connectivité pour tests

import requests
import sco_version
import app.scodoc.sco_utils as scu

response = requests.get(
    scu.SCO_UP2DATE + "/" + sco_version.SCOVERSION, timeout=scu.SCO_ORG_TIMEOUT
)

print(response.status_code)
print(response.text)
