#!/bin/bash

# Lance les tests unitaires de l'API
# Ce script lance un serveur scodoc sur le port 5555
# attend qu'il soit initialisé puis lance les tests client API.
#
# On peut aussi le lancer avec l'option --dont-start-server
# auquel cas il utilise un serveur existant, qui doit avoir été lancé
# par ailleurs, par exemple via le script:
#     tests/api/start_api_server.sh -p 5555
#
# Toutes les autres options sont passées telles qu'elles à pytest
#
# Utilisation pour générer des exemples de documentation:
#   tools/test_api.sh --make-samples
#
# Exemples:
# - lancer tous les tests API: tools/test_api.sh
# - lancer tous les tests, en mode debug (arrêt pdb sur le 1er):
#       tools/test_api.sh -x --pdb tests/api
# - lancer un module de test, en utilisant un server dev existant:
#       tools/test_api.sh --dont-start-server  -x --pdb tests/api/test_api_evaluations.py
# - Générer les samples pour la doc:
#       tools/test_api.sh --make-samples
#
# E. Viennet, Fev 2023

cd /opt/scodoc || exit 1
# suppose que le virtual env est bien configuré

# Utilise un port spécifique pour pouvoir lancer ce test sans couper
# le serveur de dev
PORT=5555
SERVER_LOG=/tmp/test_api_server.log

export SCODOC_URL="http://localhost:${PORT}"

if [ "$1" = "--dont-start-server" ]
then
  START_SERVER=0
  shift
  echo "Using existing scodoc server on port $PORT"
else
  START_SERVER=1
fi

if [ "$START_SERVER" -eq 1 ]
then
  # ------- Check pas de serveur déjà lancé
  if nc -z localhost "$PORT"
  then
      fuser -v "$PORT"/tcp
      echo Server already running on port "$PORT"
      echo You may want to try: fuser -k "$PORT"/tcp
      echo aborting tests
      exit 1
  fi

  tests/api/start_api_server.sh -p "$PORT" &> "$SERVER_LOG" &
  pid=$!
  echo "ScoDoc test server logs are in $SERVER_LOG"
  # Wait for server setup
  echo -n "Waiting for server"
  while ! nc -z localhost "$PORT"; do
    echo -n .
    sleep 1
  done
  echo
  echo Server PID "$pid" running on port "$PORT"
  # ------------------
fi

if [ "$#" -eq 0 ]
then
  echo "Starting pytest tests/api"
  pytest tests/api
else
  if [ "$1" = "--make-samples" ]
  then
    echo "Generating API documentation samples"
    python tests/api/make_samples.py -i /tmp/samples.csv
  else
    echo "Starting pytest $@"
    pytest "$@"
  fi
fi

# ------------------
if [ "$START_SERVER" -eq 1 ]
then
  echo "Killing test server"
  kill "$pid"
  fuser -k "$PORT"/tcp
fi





