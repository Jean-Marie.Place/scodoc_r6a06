"""ValidationDUT120

Revision ID: 9794534db935
Revises: 60119446aab6
Create Date: 2024-07-06 17:36:41.576748

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "9794534db935"
down_revision = "60119446aab6"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "validation_dut120",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("etudid", sa.Integer(), nullable=False),
        sa.Column("formsemestre_id", sa.Integer(), nullable=False),
        sa.Column("referentiel_competence_id", sa.Integer(), nullable=False),
        sa.Column(
            "date",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(["etudid"], ["identite.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(
            ["formsemestre_id"],
            ["notes_formsemestre.id"],
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["referentiel_competence_id"],
            ["apc_referentiel_competences.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("validation_dut120", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_validation_dut120_etudid"), ["etudid"], unique=False
        )

    # ### end Alembic commands ###


def downgrade():
    with op.batch_alter_table("validation_dut120", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_validation_dut120_etudid"))

    op.drop_table("validation_dut120")
    # ### end Alembic commands ###
