"""passwd_must_be_changed

Revision ID: bcd959a23aea
Revises: 2640b7686de6
Create Date: 2024-11-01 09:51:01.299407

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "bcd959a23aea"
down_revision = "2640b7686de6"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "passwd_must_be_changed",
                sa.Boolean(),
                server_default="false",
                nullable=False,
            )
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_column("passwd_must_be_changed")
