"""Ajoute code apo RCUE sur les UEs

Revision ID: 809faa9d89ec
Revises: cddabc3f868a
Create Date: 2024-06-07 11:06:07.694166

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "809faa9d89ec"
down_revision = "cddabc3f868a"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("notes_ue", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("code_apogee_rcue", sa.String(length=512), nullable=True)
        )


def downgrade():
    with op.batch_alter_table("notes_ue", schema=None) as batch_op:
        batch_op.drop_column("code_apogee_rcue")
