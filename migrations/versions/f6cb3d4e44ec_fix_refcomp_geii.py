"""fix_refcomp_geii

Revision ID: f6cb3d4e44ec
Revises: 9794534db935
Create Date: 2024-07-10 17:03:58.549168

Élimine éventuels doublons dans associations Apprentissages critiques du ref. comp.
Pour le cas de GEII seulement.
"""

from alembic import op


# revision identifiers, used by Alembic.
revision = "f6cb3d4e44ec"
down_revision = "9794534db935"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE apc_modules_acs_temp AS
            SELECT DISTINCT module_id, app_crit_id
            FROM apc_modules_acs;
        DELETE FROM apc_modules_acs;
        INSERT INTO apc_modules_acs (module_id, app_crit_id)
            SELECT module_id, app_crit_id
            FROM apc_modules_acs_temp;
        """
    )
    with op.batch_alter_table("apc_modules_acs", schema=None) as batch_op:
        batch_op.create_unique_constraint(
            "uix_module_id_app_crit_id", ["module_id", "app_crit_id"]
        )

    op.drop_table("apc_modules_acs_temp")


def downgrade():
    with op.batch_alter_table("apc_modules_acs", schema=None) as batch_op:
        batch_op.drop_constraint("uix_module_id_app_crit_id", type_="unique")
