"""add dispositif_descr

Revision ID: bc85a55e63e1
Revises: bcd959a23aea
Create Date: 2024-12-30 18:32:55.024694

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "bc85a55e63e1"
down_revision = "bcd959a23aea"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table(
        "notes_formsemestre_description", schema=None
    ) as batch_op:
        batch_op.add_column(
            sa.Column("dispositif_descr", sa.Text(), server_default="", nullable=False)
        )


def downgrade():
    with op.batch_alter_table(
        "notes_formsemestre_description", schema=None
    ) as batch_op:
        batch_op.drop_column("dispositif_descr")
