"""FormSemestreDescription et capacité d'accueil

Revision ID: 2640b7686de6
Revises: f6cb3d4e44ec
Create Date: 2024-08-11 15:44:32.560054

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "2640b7686de6"
down_revision = "f6cb3d4e44ec"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "notes_formsemestre_description",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("description", sa.Text(), server_default="", nullable=False),
        sa.Column("horaire", sa.Text(), server_default="", nullable=False),
        sa.Column("date_debut_inscriptions", sa.DateTime(timezone=True), nullable=True),
        sa.Column("date_fin_inscriptions", sa.DateTime(timezone=True), nullable=True),
        sa.Column("wip", sa.Boolean(), server_default="false", nullable=False),
        sa.Column("image", sa.LargeBinary(), nullable=True),
        sa.Column("campus", sa.Text(), server_default="", nullable=False),
        sa.Column("salle", sa.Text(), server_default="", nullable=False),
        sa.Column("dispositif", sa.Integer(), server_default="0", nullable=False),
        sa.Column("modalites_mcc", sa.Text(), server_default="", nullable=False),
        sa.Column("photo_ens", sa.LargeBinary(), nullable=True),
        sa.Column("public", sa.Text(), server_default="", nullable=False),
        sa.Column("prerequis", sa.Text(), server_default="", nullable=False),
        sa.Column("responsable", sa.Text(), server_default="", nullable=False),
        sa.Column("formsemestre_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["formsemestre_id"], ["notes_formsemestre.id"], ondelete="CASCADE"
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.add_column(sa.Column("capacite_accueil", sa.Integer(), nullable=True))


def downgrade():
    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.drop_column("capacite_accueil")
    op.drop_table("notes_formsemestre_description")
