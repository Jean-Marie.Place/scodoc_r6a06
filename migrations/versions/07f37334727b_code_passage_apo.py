"""code_passage_apo

Revision ID: 07f37334727b
Revises: 809faa9d89ec
Create Date: 2024-06-24 02:15:54.019156

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "07f37334727b"
down_revision = "809faa9d89ec"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.add_column(sa.Column("elt_passage_apo", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.drop_column("elt_passage_apo")
