"""justif_validate_permission

Revision ID: 60119446aab6
Revises: 07f37334727b
Create Date: 2024-06-26 16:20:12.283501

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "60119446aab6"
down_revision = "07f37334727b"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # ### end Alembic commands ###
    # Modification des permissions API
    # AbsChange (1 << 13) = 8192
    # JustifValidate (1 << 51 ) = 2251799813685248
    op.execute(
        """
        update role set permissions = permissions | 2251799813685248 where (permissions & 8192) <> 0;
        """
    )


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # ### end Alembic commands ###
    pass
