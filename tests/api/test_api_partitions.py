# -*- coding: utf-8 -*-

"""Test API : groupes et partitions

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_partitions.py
"""

from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    GET,
    POST,
    api_headers,
)
from tests.api.tools_test_api import (
    verify_fields,
    PARTITIONS_FIELDS,
    PARTITION_GROUPS_ETUD_FIELDS,
)


def test_partition(api_headers):
    """
    Test 'partition'

    Route :
        - /partition/<int:partition_id>
    """
    partition_id = 1
    partition = GET(f"/partition/{partition_id}", headers=api_headers)
    assert isinstance(partition, dict)
    assert verify_fields(partition, PARTITIONS_FIELDS) is True
    assert partition_id == partition["id"]
    assert isinstance(partition["id"], int)
    assert isinstance(partition["formsemestre_id"], int)
    assert partition["partition_name"] is None or isinstance(
        partition["partition_name"], str
    )
    assert isinstance(partition["numero"], int)
    assert isinstance(partition["bul_show_rank"], bool)
    assert isinstance(partition["show_in_lists"], bool)


def test_formsemestre_partition(api_headers):
    """Test
    /formsemestre/<int:formsemestre_id>/partitions
    /formsemestre/<int:formsemestre_id>/partition/create
    /partition/<int:partition_id>/group/create
    /partition/<int:partition_id>
    /partition/<int:partition_id/delete
    /group/<int:group_id>/set_etudiant/<int:etudid>
    """
    headers = api_headers
    formsemestre_id = 1
    partitions = GET(f"/formsemestre/{formsemestre_id}/partitions", headers=headers)
    # au départ, une partition (TD avec groupes A et B)
    assert len(partitions) == 1
    # --- Création partition
    partition_d = {
        "partition_name": "T&Dé",
        "bul_show_rank": True,
        "show_in_lists": True,
    }
    partition_r = POST(
        f"/formsemestre/{formsemestre_id}/partition/create",
        partition_d,
        headers=headers,
    )
    assert isinstance(partition_r, dict)
    assert partition_r["partition_name"] == partition_d["partition_name"]
    assert partition_r["groups"] == {}
    # --- Création Groupe
    group_d = {"group_name": "Aé-&"}
    group_r = POST(
        f"/partition/{partition_r['id']}/group/create",
        group_d,
        headers=headers,
    )
    assert isinstance(group_r, dict)
    assert group_r["group_name"] == group_d["group_name"]
    assert group_r["edt_id"] is None
    # --- Liste groupes de la partition
    partition = GET(f"/partition/{partition_r['id']}", headers=headers)
    assert isinstance(partition, dict)
    assert partition["id"] == partition_r["id"]
    assert isinstance(partition["groups"], dict)
    assert len(partition["groups"]) == 1
    group = partition["groups"][str(group_r["id"])]  # nb: str car clés json en string
    assert group["group_name"] == group_d["group_name"]

    # --- Ajout d'un groupe avec edt_id
    group_d = {"group_name": "extra", "edt_id": "GEDT"}
    group_r = POST(
        f"/partition/{partition_r['id']}/group/create",
        group_d,
        headers=headers,
    )
    assert group_r["edt_id"] == "GEDT"
    # Edit edt_id
    group_r = POST(
        f"/group/{group_r['id']}/edit",
        {"edt_id": "GEDT2"},
        headers=headers,
    )
    assert group_r["edt_id"] == "GEDT2"
    partition = GET(f"/partition/{partition_r['id']}", headers=headers)
    group = partition["groups"][str(group_r["id"])]  # nb: str car clés json en string
    assert group["group_name"] == group_d["group_name"]
    assert group["edt_id"] == "GEDT2"

    # Change edt_id via route dédiée:
    group_t = POST(
        f"/group/{group_r['id']}/set_edt_id/GEDT3",
        headers=headers,
    )
    assert group_t["id"] == group_r["id"]
    assert group_t["edt_id"] == "GEDT3"

    # Place un étudiant dans le groupe
    etud = GET(f"/formsemestre/{formsemestre_id}/etudiants", headers=headers)[0]
    repl = POST(f"/group/{group['id']}/set_etudiant/{etud['id']}", headers=headers)
    assert isinstance(repl, dict)
    assert repl["group_id"] == group["id"]
    assert repl["etudid"] == etud["id"]
    #
    etuds = GET(f"/group/{group['id']}/etudiants", headers=headers)
    assert len(etuds) == 1
    assert etuds[0]["id"] == etud["id"]
    # Retire l'étudiant du groupe
    repl = POST(f"/group/{group['id']}/remove_etudiant/{etud['id']}", headers=headers)
    assert len(GET(f"/group/{group['id']}/etudiants", headers=headers)) == 0

    # Le retire à nouveau ! (bug #465)
    repl = POST(f"/group/{group['id']}/remove_etudiant/{etud['id']}", headers=headers)
    assert repl["group_id"] == group["id"]
    # Avec partition (vérifie encodeur JSON)
    partitions = GET(f"/formsemestre/{formsemestre_id}/partitions", headers=headers)
    assert partitions
    # Delete partition
    repl = POST(f"/partition/{partition_r['id']}/delete", headers=headers)
    assert repl["OK"] is True


def test_group_etudiants(api_headers):
    """
    Test '/group/<int:group_id>/etudiants'

    Routes :
        - /group/<int:group_id>/etudiants
        - /group/<int:group_id>/etudiants/query?etat=<string:etat>
    """
    group_id = 1
    etuds = GET(f"/group/{group_id}/etudiants", headers=api_headers)
    assert isinstance(etuds, list)

    for etud in etuds:
        assert verify_fields(etud, PARTITION_GROUPS_ETUD_FIELDS)
        assert isinstance(etud["id"], int)
        assert isinstance(etud["dept_id"], int)
        assert isinstance(etud["nom"], str)
        assert isinstance(etud["prenom"], str)
        assert etud["nom_usuel"] is None or isinstance(etud["nom_usuel"], str)
        assert isinstance(etud["civilite"], str)
        assert isinstance(etud["code_nip"], str)
        assert isinstance(etud["code_ine"], str)

    # query sans filtre:
    etuds_query = GET(f"/group/{group_id}/etudiants/query", headers=api_headers)
    assert etuds_query == etuds

    etat = "I"
    etuds = GET(f"/group/{group_id}/etudiants/query?etat={etat}", headers=api_headers)
    assert isinstance(etuds, list)
    for etud in etuds:
        assert verify_fields(etud, PARTITION_GROUPS_ETUD_FIELDS)
        assert isinstance(etud["id"], int)
        assert isinstance(etud["dept_id"], int)
        assert isinstance(etud["nom"], str)
        assert isinstance(etud["prenom"], str)
        assert etud["nom_usuel"] is None or isinstance(etud["nom_usuel"], str)
        assert isinstance(etud["civilite"], str)


# # set_groups
# def test_set_groups(api_headers):
#     """
#     Test 'set_groups'
#
#     Routes :
#         - /partitions/set_groups/partition/<int:partition_id>/groups/<string:groups_id>/delete/<string:groups_to_delete>"
#           "/create/<string:groups_to_create>
#     """
#     r = requests.get(
#         SCODOC_URL
#         + "/partitions/set_groups/partition/<int:partition_id>/groups/<string:groups_id>"
#           "/delete/<string:groups_to_delete>/create/<string:groups_to_create>",
#         headers=api_headers,
#         verify=CHECK_CERTIFICATE,
#     )
#     assert r.status_code == 200
