#!/usr/bin/env python3
# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Exemple utilisation API ScoDoc 9

    Usage:
        cd /opt/scodoc/tests/api
        python -i dump_all_results.py

    Demande les résultats (bulletins JSON) de tous les semestres
    et les enregistre dans un fichier json
    (`all_results.json` du rép. courant)

    Sur M1 en 9.6.966, processed 1219 formsemestres in 645.64s
    Sur M1 en 9.6.967, processed 1219 formsemestres in 626.22s

"""

import json
from pprint import pprint as pp
import sys
import time
import urllib3
from setup_test_api import (
    API_PASSWORD,
    API_URL,
    API_USER,
    APIError,
    CHECK_CERTIFICATE,
    get_auth_headers,
    GET,
    POST,
    SCODOC_URL,
)


if not CHECK_CERTIFICATE:
    urllib3.disable_warnings()

print(f"SCODOC_URL={SCODOC_URL}")
print(f"API URL={API_URL}")


HEADERS = get_auth_headers(API_USER, API_PASSWORD)


# Liste de tous les formsemestres (de tous les depts)
formsemestres = GET("/formsemestres/query", headers=HEADERS)
print(f"{len(formsemestres)} formsemestres")

#
all_results = []
t0 = time.time()
for formsemestre in formsemestres:
    print(f"{formsemestre['session_id']}\t", end="", flush=True)
    t = time.time()
    result = GET(f"/formsemestre/{formsemestre['id']}/resultats", headers=HEADERS)
    print(f"{(time.time()-t):3.2f}s")
    all_results.append(
        f"""
{{
    "formsemestre" : { json.dumps(formsemestre, indent=1, sort_keys=True) },
    "resultats" : { json.dumps(result, indent=1, sort_keys=True)  }
}}
    """
    )

print(f"Processed {len(formsemestres)} formsemestres in {(time.time()-t0):3.2f}s")

with open("all_results.json", "w", encoding="utf-8") as f:
    f.write("[\n" + ",\n".join(all_results) + "\n]")
