"""Utilitaires pour les tests de l'API
"""

import json


def verify_fields(json_response: dict, expected_fields: set) -> bool:
    """
    Vérifie si les champs attendu de la réponse json sont présents

    json_response : la réponse de la requête
    expected_fields : ensemble des champs à vérifier

    Retourne True ou False
    """
    return all(field in json_response for field in expected_fields)


def verify_occurences_ids_etuds(json_response) -> bool:
    """
    Vérifie si il n'y a pas deux fois le même id dans la liste d'étudiant donnée en paramètres

    json_response : la réponse de la requête

    Retourne True ou False
    """
    etuds = json.loads(json_response)

    ids = [etud["id"] for etud in etuds]
    nips = [etud["code_nip"] for etud in etuds]
    ines = [etud["code_ine"] for etud in etuds]

    return (
        (len(set(ids)) == len(ids))
        and (len(set(nips)) == len(nips))
        and (len(set(ines)) == len(ines))
    )


DEPARTEMENT_FIELDS = [
    "acronym",
    "date_creation",
    "description",
    "id",
    "visible",
]

# Champs "données personnelles"
ETUD_FIELDS_RESTRICTED = {
    "boursier",
}
ETUD_FIELDS = {
    "admission",
    "adresses",
    "civilite",
    "code_ine",
    "code_nip",
    "date_naissance",
    "dept_acronym",
    "dept_id",
    "dept_naissance",
    "id",
    "lieu_naissance",
    "nationalite",
    "nom",
    "prenom",
} | ETUD_FIELDS_RESTRICTED


FORMATION_FIELDS = {
    "acronyme",
    "code_specialite",
    "dept_id",
    "formation_code",
    "formation_id",
    "id",
    "referentiel_competence_id",
    "titre_officiel",
    "titre",
    "type_parcours",
    "version",
}

FORMATION_EXPORT_FIELDS = {
    # "dept_id" "id" "formation_id", pas présents sauf si export_with_ids
    "acronyme",
    "code_specialite",
    "formation_code",
    "referentiel_competence_id",
    "titre_officiel",
    "titre",
    "type_parcours",
    "ue",
    "version",
}

FORMATION_EXPORT_UE_FIELDS = {
    "acronyme",
    "code_apogee",
    "coefficient",
    "color",
    "ects",
    "is_external",
    "matiere",
    "numero",
    "reference",
    "semestre_idx",
    "titre",
    "type",
    "ue_code",
}

FORMATION_EXPORT_UE_MATIERE_FIELDS = {
    "module",
    "numero",
    "titre",
}

FORMATION_EXPORT_UE_MATIERE_MODULE_FIELDS = {
    "abbrev",
    "app_critiques",
    "code_apogee",
    "code",
    "coefficient",
    "coefficients",
    "edt_id",
    "heures_cours",
    "heures_td",
    "heures_tp",
    "module_type",
    "numero",
    "parcours",
    "semestre_id",
    "titre",
}

FORMATION_EXPORT_UE_MATIERE_MODULE_COEF_FIELDS = {
    "ue_reference",
    "coef",
}

FORMSEMESTRE_FIELDS = [
    "block_moyenne_generale",
    "block_moyennes",
    "bul_bgcolor",
    "bul_hide_xml",
    "date_debut_iso",
    "date_debut",
    "date_fin_iso",
    "date_fin",
    "departement",
    "dept_id",
    "elt_annee_apo",
    "elt_sem_apo",
    "ens_can_edit_eval",
    "etape_apo",
    "etat",
    "formation_id",
    "formation",
    "formsemestre_id",
    "gestion_compensation",
    "gestion_semestrielle",
    "id",
    "modalite",
    "parcours",
    "resp_can_change_ens",
    "resp_can_edit",
    "responsables",
    "scodoc7_id",
    "semestre_id",
    "titre_formation",
    "titre_num",
    "titre",
]

FSEM_FIELDS = {
    "block_moyenne_generale",
    "block_moyennes",
    "bul_bgcolor",
    "bul_hide_xml",
    "date_debut_iso",
    "date_debut",
    "date_fin_iso",
    "date_fin",
    "dept_id",
    "elt_annee_apo",
    "elt_sem_apo",
    "ens_can_edit_eval",
    "etat",
    "formation_id",
    "formsemestre_id",
    "gestion_compensation",
    "gestion_semestrielle",
    "id",
    "modalite",
    "resp_can_change_ens",
    "resp_can_edit",
    "responsables",
    "semestre_id",
    "titre_num",
    "titre",
}

MODIMPL_FIELDS = {
    "computation_expr",
    "ens",
    "formsemestre_id",
    "id",
    "module_id",
    "module",
    "moduleimpl_id",
    "responsable_id",
}

MODULE_FIELDS = {
    "abbrev",
    "code_apogee",
    "code",
    "coefficient",
    "ects",
    "formation_id",
    "heures_cours",
    "heures_td",
    "heures_tp",
    "id",
    "matiere_id",
    "module_id",
    "module_type",
    "numero",
    "semestre_id",
    "titre",
    "ue_id",
}

UE_FIELDS = {
    "acronyme",
    "code_apogee",
    "coefficient",
    "color",
    "ects",
    "formation_id",
    "id",
    "is_external",
    "numero",
    "semestre_idx",
    "titre",
    "type",
    "ue_code",
    "ue_id",
}

BULLETIN_FIELDS = {
    "date",
    "etat_inscription",
    "etudiant",
    "formation",
    "formsemestre_id",
    "options",
    "publie",
    "ressources",
    "saes",
    "semestre",
    "type",
    "ues",
    "version",
}


BULLETIN_ETUDIANT_FIELDS = {
    "boursier",
    "civilite",
    "code_ine",
    "code_nip",
    "codepostaldomicile",
    "date_naissance",
    "dept_acronym",
    "dept_id",
    "dept_naissance",
    "description",
    "domicile",
    "email",
    "emailperso",
    "etudid",
    "fax",
    "fiche_url",
    "id",
    "lieu_naissance",
    "nationalite",
    "nom",
    "nomprenom",
    "paysdomicile",
    "photo_url",
    "prenom",
    "telephone",
    "telephonemobile",
    "typeadresse",
    "villedomicile",
}

BULLETIN_FORMATION_FIELDS = {"id", "acronyme", "titre_officiel", "titre"}

BULLETIN_OPTIONS_FIELDS = {
    "show_abs_modules",
    "show_abs",
    "show_codemodules",
    "show_coef",
    "show_date_inscr",
    "show_ects",
    "show_matieres",
    "show_minmax_eval",
    "show_minmax_mod",
    "show_minmax",
    "show_mod_rangs",
    "show_moypromo",
    "show_rangs",
    "show_temporary",
    "show_ue_cap_current",
    "show_ue_cap_details",
    "show_ue_rangs",
    "show_uevalid",
    "temporary_txt",
}

BULLETIN_RESSOURCES_FIELDS = {
    "R101",
    "R102",
    "R103",
    "R104",
    "R105",
    "R106",
    "R107",
    "R108",
    "R109",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
    "R115",
}

BULLETIN_SAES_FIELDS = {
    "SAE11",
    "SAE12",
    "SAE13",
    "SAE14",
    "SAE15",
    "SAE16",
}

########### RESSOURCES ET SAES ###########
BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_FIELDS = {
    "code_apogee",
    "evaluations",
    "id",
    "moyenne",
    "titre",
    "url",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_FIELDS = {
    "coef",
    "date",
    "description",
    "heure_debut",
    "heure_fin",
    "id",
    "note",
    "poids",
    "url",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_POIDS_FIELDS = {
    "RT1.1",
    "RT2.1",
    "RT3.1",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_NOTE_FIELDS = {
    "max",
    "min",
    "moy",
    "value",
}


########### UES ###########
BULLETIN_UES_FIELDS = {"RT1.1", "RT2.1", "RT3.1"}

BULLETIN_UES_UE_FIELDS = {
    "bonus",
    "capitalise",
    "color",
    "competence",
    "ECTS",
    "id",
    "malus",
    "moyenne",
    "numero",
    "ressources",
    "saes",
    "titre",
    "type",
}

BULLETIN_UES_UE_MOYENNE_FIELDS = {"value", "min", "max", "moy", "rang", "total"}

BULLETIN_UES_RT11_RESSOURCES_FIELDS = {
    "R101",
    "R102",
    "R103",
    "R104",
    "R106",
    "R108",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
}

BULLETIN_UES_RT21_RESSOURCES_FIELDS = {
    "R101",
    "R103",
    "R104",
    "R105",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
    "R115",
}

BULLETIN_UES_RT31_RESSOURCES_FIELDS = {
    "R101",
    "R107",
    "R108",
    "R109",
    "R110",
    "R111",
    "R112",
    "R115",
}

BULLETIN_UES_UE_RESSOURCES_RESSOURCE_FIELDS = {"id", "coef", "moyenne"}

BULLETIN_UES_RT11_SAES_FIELDS = {
    "SAE11",
    "SAE12",
}

BULLETIN_UES_RT21_SAES_FIELDS = {"SAE13"}

BULLETIN_UES_RT31_SAES_FIELDS = {
    "SAE14",
    "SAE15",
}

BULLETIN_UES_UE_SAES_SAE_FIELDS = {"id", "coef", "moyenne"}


BULLETIN_UES_UE_ECTS_FIELDS = {"acquis", "total"}


########### SEMESTRE ###########
BULLETIN_SEMESTRE_FIELDS = {
    "absences",
    "annee_universitaire",
    "date_debut",
    "date_fin",
    "ECTS",
    "etapes",
    "groupes",
    "inscription",
    "notes",
    "numero",
    "rang",
}

BULLETIN_SEMESTRE_ABSENCES_FIELDS = {"injustifie", "total"}

BULLETIN_SEMESTRE_ECTS_FIELDS = {"acquis", "total"}

BULLETIN_SEMESTRE_NOTES_FIELDS = {"value", "min", "moy", "max"}

BULLETIN_SEMESTRE_RANG_FIELDS = {"value", "total"}


EVAL_FIELDS = {
    "coefficient",
    "date_debut",
    "date_fin",
    "description",
    "etat",
    "evaluation_type",
    "id",
    "moduleimpl_id",
    "nb_inscrits",
    "nb_notes_abs",
    "nb_notes_att",
    "nb_notes_exc",
    "nb_notes_manquantes",
    "note_max",
    "numero",
    "poids",
    "publish_incomplete",
    "saisie_notes",
    "visibulletin",
}

SAISIE_NOTES_FIELDS = {"datetime_debut", "datetime_fin", "datetime_mediane"}

REF_COMP_FIELDS = {
    "annexe",
    "competences",
    "dept_id",
    "parcours",
    "scodoc_date_loaded",
    "scodoc_orig_filename",
    "specialite_long",
    "specialite",
    "type_departement",
    "type_structure",
    "type_titre",
    "version_orebut",
}

ABSENCES_FIELDS = {
    "begin",
    "description",
    "end",
    "estabs",
    "estjust",
    "jour",
    "matin",
}

ABSENCES_GROUP_ETAT_FIELDS = {"etudid", "list_abs"}


FORMSEMESTRE_ETUD_FIELDS = {
    "civilite",
    "code_ine",
    "code_nip",
    "groups",
    "id",
    "nom_usuel",
    "nom",
    "prenom",
}

FORMSEMESTRE_ETUS_GROUPS_FIELDS = {
    "bul_show_rank",
    "formsemestre_id",
    "group_id",
    "group_name",
    "id",
    "numero",
    "partition_id",
    "partition_name",
    "show_in_lists",
}

EVALUATIONS_FIELDS = {
    "coefficient",
    "date_debut",
    "date_fin",
    "description",
    "evaluation_type",
    "id",
    "note_max",
    "numero",
    "poids",
    "publish_incomplete",
    "visibulletin",
}

NOTES_FIELDS = {
    "comment",
    "date",
    "etudid",
    "evaluation_id",
    "uid",
    "value",
}


PARTITIONS_FIELDS = {
    "bul_show_rank",
    "formsemestre_id",
    "id",
    "numero",
    "partition_name",
    "show_in_lists",
}

PARTITION_GROUPS_ETUD_FIELDS = {
    "civilite",
    "code_ine",
    "code_nip",
    "dept_id",
    "id",
    "nom_usuel",
    "nom",
    "prenom",
}

FORMSEMESTRE_BULLETINS_FIELDS = {
    "date",
    "etat_inscription",
    "etudiant",
    "formation",
    "formsemestre_id",
    "options",
    "publie",
    "ressources",
    "saes",
    "semestre",
    "type",
    "ues",
    "version",
}

FORMSEMESTRE_BULLETINS_ETU_FIELDS = {
    "boursier",
    "civilite",
    "code_ine",
    "code_nip",
    "codepostaldomicile",
    "date_naissance",
    "dept_acronym",
    "dept_id",
    "dept_naissance",
    "description",
    "domicile",
    "email",
    "emailperso",
    "etudid",
    "fax",
    "fiche_url",
    "id",
    "lieu_naissance",
    "nationalite",
    "nom",
    "nomprenom",
    "paysdomicile",
    "photo_url",
    "prenom",
    "telephone",
    "telephonemobile",
    "typeadresse",
    "villedomicile",
}

FORMSEMESTRE_BULLETINS_FORMATION_FIELDS = {
    "acronyme",
    "id",
    "titre_officiel",
    "titre",
}

FORMSEMESTRE_BULLETINS_OPT_FIELDS = {
    "show_abs_modules",
    "show_abs",
    "show_codemodules",
    "show_coef",
    "show_date_inscr",
    "show_ects",
    "show_matieres",
    "show_minmax_eval",
    "show_minmax_mod",
    "show_minmax",
    "show_mod_rangs",
    "show_moypromo",
    "show_rangs",
    "show_temporary",
    "show_ue_cap_current",
    "show_ue_cap_details",
    "show_ue_rangs",
    "show_uevalid",
    "temporary_txt",
}
