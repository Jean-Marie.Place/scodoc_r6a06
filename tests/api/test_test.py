# -*- coding: UTF-8 -*

"""Unit tests for... tests

Ensure test DB is in the expected initial state.

Usage: pytest tests/unit/test_test.py
"""

import pytest

from tests.api.setup_test_api import (
    api_headers,
    GET,
)


@pytest.mark.test_test
def test_test_db(api_headers):
    """Check that we indeed have: 2 users, 1 dept, 3 formsemestres.
    Juste après init, les ensembles seront ceux donnés ci-dessous.
    Les autres tests peuvent ajouter des éléments, c'edt pourquoi on utilise issubset().
    """
    headers = api_headers
    assert {
        "admin_api",
        "admin",
        "lecteur_api",
        "other",
        "test",
        "u_AA",
        "u_BB",
        "u_CC",
        "u_DD",
        "u_TAPI",
    }.issubset({u["user_name"] for u in GET("/users/query", headers=headers)})
    assert {
        "AA",
        "BB",
        "CC",
        "DD",
        "TAPI",
    }.issubset({d["acronym"] for d in GET("/departements", headers=headers)})
    assert 1 in (
        formsemestre["semestre_id"]
        for formsemestre in GET("/formsemestres/query", headers=headers)
    )
