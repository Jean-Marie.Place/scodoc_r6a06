"""Test API exceptions
"""

import json
import requests

import pytest
from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    api_headers,
)
from app.scodoc import sco_utils as scu


def test_exceptions(api_headers):
    """
    Vérifie que les exceptions de l'API sont toutes en JSON.
    """
    # Une requete sur une url inexistante ne passe pas par les blueprints API
    # et est donc en HTML
    r = requests.get(
        f"{API_URL}/mmm/non/existant/mmm",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404
    assert r.headers["Content-Type"] == "text/html; charset=utf-8"

    # Une requete d'un objet non existant est en JSON
    r = requests.get(
        f"{API_URL}/formsemestre/999999",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404
    assert r.headers["Content-Type"] == "application/json"
    assert r.json()

    # Une requête API sans autorisation est en JSON
    r = requests.post(
        f"{API_URL}/formsemestre/1/etudid/1/inscrit",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 401
    assert r.headers["Content-Type"] == "application/json"
    assert r.json()
