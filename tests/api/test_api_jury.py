# -*- coding: utf-8 -*-

"""Test Logos

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_jury.py
"""
import requests

from app.scodoc import sco_utils as scu
from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    GET,
    POST,
    api_headers,
)


def test_jury_decisions(api_headers):
    """
    Route :
        - /formsemestre/<int:formsemestre_id>/decisions_jury
    """
    formsemestre_id = 1
    etudiants = GET(f"/formsemestre/{formsemestre_id}/etudiants", headers=api_headers)
    decisions_jury = GET(
        f"/formsemestre/{formsemestre_id}/decisions_jury", headers=api_headers
    )
    assert len(etudiants) > 0
    assert len(etudiants) == len(decisions_jury)
    # TODO La suite de ce test est a compléter: il faut modifier le formation test RT
    # pour avoir au moins le S2 et le S2: actuellement seulement le S1
    # # Récupère la formation de ce semestre pour avoir les UEs
    # r = requests.get(
    #     API_URL + "/formation/1/export",
    #     headers=api_headers,
    #     verify=CHECK_CERTIFICATE,
    #     timeout=scu.SCO_TEST_API_TIMEOUT,
    # )
    # assert r.status_code == 200
    # export_formation = r.json()
    # ues = export_formation["ue"]
    # # Enregistre une validation d'RCUE
    # etudid = etudiants[0]["id"]
    # validation = POST(
    #     f"/etudiant/{etudid}/jury/validation_rcue/record",
    #     data={
    #         "code": "ADM",
    #         "ue1_id": XXX,
    #         "ue2_id": XXX,
    #     },
    #     headers=api_headers,
    # )
    # assert validation
