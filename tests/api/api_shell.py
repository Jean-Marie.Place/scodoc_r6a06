#!/usr/bin/env python

"""Script pour tester l'API en mode interactif

Utilisation:
```py
python -i tests/api/api_shell.py
```

"""

import pdb
from pprint import pprint as pp

from setup_test_api import (
    API_PASSWORD,
    API_URL,
    API_USER,
    APIError,
    CHECK_CERTIFICATE,
    get_auth_headers,
    GET,
    POST,
    SCODOC_URL,
    set_headers,
)

set_headers(get_auth_headers("admin_api", "admin_api"))

print(
    """
Connecté au serveur ScoDoc. Vous pouvez utiliser:
GET( route )
POST( route, data )
Exemple avec pretty print:
pp(GET("/departements")[0])
"""
)

pp(GET("/departements")[0])
