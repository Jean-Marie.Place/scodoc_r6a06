##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

""" Test jury BUT avec parcours

Ces tests sont généralement lents (construction de la base),
et donc marqués par `@pytest.mark.slow`.

Certains sont aussi marqués par @pytest.mark.lemans ou @pytest.mark.lyon
pour lancer certains tests spécifiques seulement.

Exemple utilisation spécifique:
# test sur "Lyon" seulement:
pytest --pdb -m lyon tests/unit/test_but_jury.py

pytest --pdb -m but_gccd tests/unit/test_but_jury.py
"""

import pytest
from tests.unit import yaml_setup, yaml_setup_but

import app
from app.but.jury_but_validation_auto import formsemestre_validation_auto_but
from app.models import Formation, FormSemestre, UniteEns
from app.scodoc import sco_prepajury
from config import TestConfig

DEPT = TestConfig.DEPT_TEST


def setup_and_test_jurys(yaml_filename: str) -> FormSemestre | None:
    """Charge YAML et lance test jury BUT.
    Rennvoie le dernier formsemestre s'il y en a un.
    """
    app.set_sco_dept(DEPT)
    # Construit la base de test GB une seule fois
    # puis lance les tests de jury
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(yaml_filename)
    formsemestre = None
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
        # Vérifie les champs de DecisionsProposeesAnnee de ce semestre
        yaml_setup_but.check_deca_fields(formsemestre)

        # Saisie de toutes les décisions de jury "automatiques"
        # et vérification des résultats attendus:
        formsemestre_validation_auto_but(formsemestre, only_adm=False)
        yaml_setup_but.but_test_jury(formsemestre, doc)
    return formsemestre


@pytest.mark.slow
@pytest.mark.but_gb
def test_but_jury_GB(test_client):
    """Tests sur un cursus GB
    - construction des semestres et de leurs étudiants à partir du yaml
    - vérification jury de S1
    - vérification jury de S2
    - vérification jury de S3
    - vérification jury de S1 avec redoublants et capitalisations
    puis:
    - teste feuille prepajury
    """
    formsemestre: FormSemestre = setup_and_test_jurys(
        "tests/ressources/yaml/cursus_but_gb.yaml"
    )
    # Test feuille préparation jury
    sco_prepajury.feuille_preparation_jury(formsemestre.id)


@pytest.mark.slow
@pytest.mark.lemans
def test_but_jury_GMP_lm(test_client):
    """Tests sur un cursus GMP fourni par Le Mans"""
    setup_and_test_jurys("tests/ressources/yaml/cursus_but_gmp_iutlm.yaml")


@pytest.mark.slow
@pytest.mark.lyon
def test_but_jury_GEII_lyon(test_client):
    """Tests sur un cursus GEII fourni par Lyon"""
    setup_and_test_jurys("tests/ressources/yaml/cursus_but_geii_lyon.yaml")


@pytest.mark.slow
@pytest.mark.but_gccd
def test_but_jury_GCCD_CY(test_client):
    """Tests sur un cursus BUT GCCD de S1 à S6"""
    # WIP
    app.set_sco_dept(DEPT)
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/cursus_but_gccd_cy.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        _ = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )

    formsemestres = FormSemestre.query.order_by(
        FormSemestre.date_debut, FormSemestre.semestre_id
    ).all()

    formation: Formation = formsemestres[0].formation
    # Vérifie les UEs du parcours BAT
    parcour_BAT = formation.referentiel_competence.parcours.filter_by(
        code="BAT"
    ).first()
    assert parcour_BAT
    # check le nombre d'UE dans chaque semestre BUT:
    assert [
        len(
            formation.query_ues_parcour(parcour_BAT)
            .filter(UniteEns.semestre_idx == i)
            .all()
        )
        for i in range(1, 7)
    ] == [5, 5, 5, 5, 3, 3]
    # Vérifie les UEs du parcours TP
    parcour_TP = formation.referentiel_competence.parcours.filter_by(code="TP").first()
    assert parcour_TP
    # check le nombre d'UE dans chaque semestre BUT:
    assert [
        len(
            formation.query_ues_parcour(parcour_TP)
            .filter(UniteEns.semestre_idx == i)
            .all()
        )
        for i in range(1, 7)
    ] == [5, 5, 5, 5, 3, 3]
