""" Test méthodes génériques sur les modèles ScoDoc
"""

import pytest

from flask import g

import app
from app import db
from app.models import Departement, GroupDescr

from config import TestConfig
from tests.unit import yaml_setup

DEPT = TestConfig.DEPT_TEST


def test_get_instance(test_client):
    """Test accès instance avec ou sans dept"""
    assert DEPT
    app.set_sco_dept(DEPT)
    # Création d'un autre départment
    other_dept = Departement(acronym="X666")
    db.session.add(other_dept)
    db.session.flush()
    # Crée qq semestres...
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/simple_formsemestres.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
        assert formsemestre
    # prend l'exemple de GroupDescr
    # Crée un formsemestre et un groupe
    gr = GroupDescr.query.first()
    assert gr
    oid = gr.id
    # Accès sans département (comme le fait l'API non départementale)
    g.scodoc_dept = None
    g.scodoc_dept_id = -1  # invalide
    assert GroupDescr.get_instance(oid, accept_none=True).id == oid
    # Accès avec le bon département
    app.set_sco_dept(DEPT)
    assert GroupDescr.get_instance(oid, accept_none=True).id == oid
    # Accès avec un autre département
    app.set_sco_dept(other_dept.acronym)
    assert GroupDescr.get_instance(oid, accept_none=True) is None
