"""Test calculs rattrapages
"""

import datetime

import app
from app import db
from app.comp import res_sem
from app.comp.res_but import ResultatsSemestreBUT
from app.models import Evaluation, FormSemestre, ModuleImpl
from app.scodoc import (
    sco_bulletins,
    sco_formsemestre,
    sco_saisie_notes,
)
from app.scodoc import sco_utils as scu
from config import TestConfig
from tests.unit import sco_fake_gen

DEPT = TestConfig.DEPT_TEST


def test_notes_rattrapage(test_client):
    """Test quelques opérations élémentaires de ScoDoc
    Création 1 étudiant, formation, semestre, inscription etudiant,
    creation 1 evaluation, saisie notes.
    """
    app.set_sco_dept(DEPT)

    G = sco_fake_gen.ScoFake(verbose=False)
    etuds = [G.create_etud(code_nip=None)]  # un seul

    formation_id = G.create_formation(acronyme="")
    ue_id = G.create_ue(formation_id=formation_id, acronyme="TST1", titre="ue test")
    matiere_id = G.create_matiere(ue_id=ue_id, titre="matière test")
    module_id = G.create_module(
        matiere_id=matiere_id,
        code="TSM1",
        coefficient=1.0,
        titre="module test",
    )

    # --- Mise place d'un semestre
    formsemestre_id = G.create_formsemestre(
        formation_id=formation_id,
        semestre_id=1,
        date_debut="01/01/2020",
        date_fin="30/06/2020",
    )
    sem = sco_formsemestre.get_formsemestre(formsemestre_id)
    moduleimpl_id = G.create_moduleimpl(
        module_id=module_id,
        formsemestre_id=formsemestre_id,
    )

    # --- Inscription des étudiants
    for etud in etuds:
        G.inscrit_etudiant(formsemestre_id, etud)
    # --- Creation évaluation
    e = G.create_evaluation(
        moduleimpl_id=moduleimpl_id,
        date_debut=datetime.datetime(2020, 1, 1),
        description="evaluation test",
        coefficient=1.0,
    )
    # --- Création d'une évaluation "de rattrapage"
    e_rat = G.create_evaluation(
        moduleimpl_id=moduleimpl_id,
        date_debut=datetime.datetime(2020, 1, 2),
        description="evaluation rattrapage",
        coefficient=1.0,
        evaluation_type=Evaluation.EVALUATION_RATTRAPAGE,
    )
    etud = etuds[0]
    _ = G.create_note(evaluation_id=e["id"], etudid=etud["etudid"], note=12.0)
    _ = G.create_note(evaluation_id=e_rat["id"], etudid=etud["etudid"], note=11.0)

    # --- Vérifications internes structures ScoDoc
    formsemestre = db.session.get(FormSemestre, formsemestre_id)
    res: ResultatsSemestreBUT = res_sem.load_formsemestre_results(formsemestre)
    mod_res = res.modimpls_results[moduleimpl_id]
    moduleimpl = db.session.get(ModuleImpl, moduleimpl_id)
    # retrouve l'éval. de rattrapage:
    evals_rat = mod_res.get_evaluations_rattrapage(moduleimpl)
    assert len(evals_rat) == 1
    assert evals_rat[0].id == e_rat["id"]
    # Les deux évaluations sont considérées comme complètes:
    assert len(mod_res.get_evaluations_completes(moduleimpl)) == 2

    # --- Vérification sur bulletin
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Vérifie structure du bulletin:
    assert b["etudid"] == etud["etudid"]
    assert len(b["ues"][0]["modules"][0]["evaluations"]) == 2
    assert len(b["ues"][0]["modules"]) == 1
    # Note moyenne: ici le ratrapage est inférieur à la note:
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(12.0)
    # rattrapage > moyenne:
    _ = G.create_note(evaluation_id=e_rat["id"], etudid=etud["etudid"], note=18.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(18.0)
    # rattrapage vs absences
    _ = G.create_note(evaluation_id=e["id"], etudid=etud["etudid"], note=None)  # abs
    _ = G.create_note(evaluation_id=e_rat["id"], etudid=etud["etudid"], note=17.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(17.0)
    # et sans note de rattrapage
    _ = G.create_note(evaluation_id=e["id"], etudid=etud["etudid"], note=10.0)
    _ = G.create_note(evaluation_id=e_rat["id"], etudid=etud["etudid"], note=None)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(10.0)

    # Supprime toutes les notes de l'évaluation de rattrapage:
    sco_saisie_notes.evaluation_suppress_alln(e_rat["id"], dialog_confirmed=True)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Vérifie structure du bulletin:
    assert b["etudid"] == etud["etudid"]
    assert len(b["ues"][0]["modules"][0]["evaluations"]) == 1  # reste une eval normale
    assert len(b["ues"][0]["modules"]) == 1
    # Note moyenne: reviens à 10/20
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(10.0)
    # Supprime l'évaluation de rattrapage:
    evaluation = db.session.get(Evaluation, e_rat["id"])
    assert evaluation
    evaluation.delete()
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    assert len(b["ues"][0]["modules"][0]["evaluations"]) == 1  # reste une eval normale

    # Création évaluation session 2:
    e_session2 = G.create_evaluation(
        moduleimpl_id=moduleimpl_id,
        date_debut=datetime.datetime(2020, 1, 2),
        description="evaluation session 2",
        coefficient=1.0,
        evaluation_type=Evaluation.EVALUATION_SESSION2,
    )

    res: ResultatsSemestreBUT = res_sem.load_formsemestre_results(formsemestre)
    mod_res = res.modimpls_results[moduleimpl_id]
    # retrouve l'éval. de session 2:
    evals_session2 = mod_res.get_evaluations_session2(moduleimpl)
    assert len(evals_session2) == 1
    assert evals_session2[0].id == e_session2["id"]
    # Les deux évaluations sont considérées comme complètes:
    assert len(mod_res.get_evaluations_completes(moduleimpl)) == 2

    # Saisie note session 2:
    _ = G.create_note(evaluation_id=e_session2["id"], etudid=etud["etudid"], note=5.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne: utilise session 2 même si inférieure
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(5.0)

    _ = G.create_note(evaluation_id=e_session2["id"], etudid=etud["etudid"], note=20.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne: utilise session 2 même si inférieure
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(20.0)

    # Met la note session2 à ABS (None)
    _ = G.create_note(evaluation_id=e_session2["id"], etudid=etud["etudid"], note=None)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne: zéro car ABS
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(0.0)
    # Supprime note session 2
    _ = G.create_note(
        evaluation_id=e_session2["id"], etudid=etud["etudid"], note=scu.NOTES_SUPPRESS
    )
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne: revient à sa valeur initiale, 10/20
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(10.0)
    # Supprime évaluation session 2
    evaluation = db.session.get(Evaluation, e_session2["id"])
    assert evaluation
    evaluation.delete()
    #
    # --- Evaluation bonus ---
    #
    # --- Création d'une évaluation "bonus"
    e_bonus = G.create_evaluation(
        moduleimpl_id=moduleimpl_id,
        date_debut=datetime.datetime(2020, 1, 2),
        description="evaluation bonus",
        coefficient=1.0,
        evaluation_type=Evaluation.EVALUATION_BONUS,
    )
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne sans bonus
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(10.0)
    # Saisie note bonus
    _ = G.create_note(evaluation_id=e_bonus["id"], etudid=etud["etudid"], note=1.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    # Note moyenne sans bonus
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(11.0)
    # Négatif, avec clip à zéro
    _ = G.create_note(evaluation_id=e_bonus["id"], etudid=etud["etudid"], note=-20.0)
    b = sco_bulletins.formsemestre_bulletinetud_dict(
        sem["formsemestre_id"], etud["etudid"]
    )
    assert b["ues"][0]["modules"][0]["mod_moy_txt"] == scu.fmt_note(0.0)
