# -*- coding: utf-8 -*-

"""Test Periodes

Utiliser comme:
    pytest tests/unit/_test_periode.py

Calcule la session associée à un formsemestre sous la forme (année, période)
année: première année de l'année scolaire
période = 1 (première période de l'année scolaire) ou 2 (deuxième période de l'année scolaire)
les quatre derniers paramètres forment les dates pivots pour l'année (1er août par défaut)
et pour la période (1er décembre par défaut).
Tous les calculs se font à partir de la date de début du formsemestre.
Exemples:
Début FormSemestre    pivot_année   pivot_periode     Résultat
01/01/2022             ( 1, 8)        ( 1,12)       (2021,2) # A: printemps nord
01/09/2022             ( 1, 8)        ( 1,12)       (2022,1) # B: automne nord
15/12/2022             ( 1, 8)        ( 1,12)       (2022,2)
30/07/2023             ( 1, 8)        ( 1,12)       (2022,2)
01/01/2022             ( 1, 1)        ( 1, 8)       (2022,1) # antipodes
30/07/2022             ( 1, 1)        ( 1, 8)       (2022,1) # antipodes
02/08/2022             ( 1, 1)        ( 1, 8)       (2022,2) # antipodes
30/12/2022             ( 1, 1)        ( 1, 8)       (2022,2) # antipodes
01/01/2022             ( 3, 1)        ( 1, 8)       (2021,2) # antipodes l'année sco démarre le 3 janvier
10/01/2024             ( 1, 8)        ( 1, 2)       (2023,2) # pivot période < pivot année
01/06/2024             ( 1, 8)        ( 1, 2)       (2023,2) # pivot période < pivot année
20/09/2024             ( 1, 8)        ( 1, 2)       (2024,1) # pivot période < pivot année
"""

import datetime

from app.models import FormSemestre
from app.scodoc.sco_formsemestre import sem_in_semestre_scolaire

# Nota: pour accélerer (éviter de recrer l'app),
# on regroupe ces petits tests dans une seule fonction


def test_periode(test_client):
    "Joue tous les tests de ce module"
    _test_default()
    _test_automne_nord()
    _test_noel_nord()
    _test_ete_nord()
    _test_printemps_sud()
    _test_automne_sud()
    _test_noel_sud()
    _test_ete_sud()
    _test_nouvel_an_sud()
    _test_nouvel_an_special_pp_before_pa()
    _test_nouvel_ete_pp_before_pa()
    _test_automne_special_pp_before_pa()
    _test_sem_in_periode1_default()
    _test_sem_in_periode2_default()
    _test_sem_in_annee_default()


def _test_default():
    # with default
    assert (2021, 2) == FormSemestre.comp_periode(datetime.datetime(2022, 1, 1))
    assert (2021, 2) == FormSemestre.comp_periode(
        datetime.datetime(2022, 1, 1), 8, 12, 1, 1
    )


def _test_automne_nord():
    assert (2022, 1) == FormSemestre.comp_periode(datetime.datetime(2022, 9, 1))


def _test_noel_nord():
    assert (2022, 2) == FormSemestre.comp_periode(datetime.datetime(2022, 12, 15))


def _test_ete_nord():
    assert (2021, 2) == FormSemestre.comp_periode(datetime.datetime(2022, 7, 30))


def _test_printemps_sud():
    assert (2022, 1) == FormSemestre.comp_periode(
        datetime.datetime(2022, 1, 1), 1, 1, 1, 8
    )


def _test_automne_sud():
    assert (2022, 2) == FormSemestre.comp_periode(
        datetime.datetime(2022, 8, 2), 1, 8, 1, 1
    )


def _test_noel_sud():
    assert (2022, 2) == FormSemestre.comp_periode(
        datetime.datetime(2022, 12, 30), 1, 8, 1, 1
    )


def _test_ete_sud():
    assert (2022, 1) == FormSemestre.comp_periode(
        datetime.datetime(2022, 7, 30), 1, 8, 1, 1
    )


def _test_nouvel_an_sud():
    assert (2021, 2) == FormSemestre.comp_periode(
        datetime.datetime(2022, 1, 1), 3, 8, 1, 1
    )


def _test_nouvel_an_special_pp_before_pa():
    assert (2023, 1) == FormSemestre.comp_periode(
        datetime.datetime(2024, 1, 10), 8, 2, 1, 1
    )


def _test_nouvel_ete_pp_before_pa():
    assert (2023, 2) == FormSemestre.comp_periode(datetime.datetime(2024, 6, 1), 8, 2)


def _test_automne_special_pp_before_pa():
    assert (2024, 1) == FormSemestre.comp_periode(datetime.datetime(2024, 9, 20), 8, 2)


sem_automne = {"date_debut_iso": "2022-09-24"}
sem_nouvel_an = {"date_debut_iso": "2023-01-01"}
sem_printemps = {"date_debut_iso": "2023-03-14"}
sem_ete = {"date_debut_iso": "2023-07-11"}
sem_next_year = {"date_debut_iso": "2023-08-16"}
sem_prev_year = {"date_debut_iso": "2022-07-31"}


def _test_sem_in_periode1_default():
    assert True is sem_in_semestre_scolaire(sem_automne, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_nouvel_an, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_printemps, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_ete, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_next_year, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_prev_year, 2022, 1)


def _test_sem_in_periode2_default():
    assert False is sem_in_semestre_scolaire(sem_automne, 2022, 2)
    assert True is sem_in_semestre_scolaire(sem_nouvel_an, 2022, 2)
    assert True is sem_in_semestre_scolaire(sem_printemps, 2022, 2)
    assert True is sem_in_semestre_scolaire(sem_ete, 2022, 2)
    assert False is sem_in_semestre_scolaire(sem_next_year, 2022, 1)
    assert False is sem_in_semestre_scolaire(sem_prev_year, 2022, 1)


def _test_sem_in_annee_default():
    assert True is sem_in_semestre_scolaire(sem_automne, 2022, 0)
    assert True is sem_in_semestre_scolaire(sem_nouvel_an, 2022)
    assert True is sem_in_semestre_scolaire(sem_printemps, 2022, 0)
    assert True is sem_in_semestre_scolaire(sem_ete, 2022, 0)
    assert False is sem_in_semestre_scolaire(sem_next_year, 2022)
    assert False is sem_in_semestre_scolaire(sem_prev_year, 2022, 0)
