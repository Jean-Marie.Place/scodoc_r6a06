"""Test des fonctions utilitaires de sco_utils.py


Utiliser comme:
    pytest tests/unit/test_sco_utils.py

"""

# pylint: disable=C0111
# no doc strings in short tests

import datetime
import pytest

from app.scodoc.sco_exceptions import ScoValueError
from app.scodoc.sco_utils import convert_fr_date, heure_to_iso8601


# Dates humaines -> ISO
def test_convert_fr_date_full_date():
    assert convert_fr_date("12/2/1972") == datetime.datetime(1972, 2, 12)


def test_convert_fr_date_short_date_before_pivot():
    assert convert_fr_date("12/2/24") == datetime.datetime(2024, 2, 12)


def test_convert_fr_date_short_date_after_pivot():
    assert convert_fr_date("12/2/72") == datetime.datetime(1972, 2, 12)


def test_convert_fr_date_datetime_object():
    dt = datetime.datetime(2022, 2, 12)
    assert convert_fr_date(dt) == dt


def test_convert_fr_date_invalid_date():
    with pytest.raises(ScoValueError):
        convert_fr_date("invalid date")


def test_convert_fr_date_iso_format():
    assert convert_fr_date("2022-02-12T00:00:00") == datetime.datetime(2022, 2, 12)


def test_convert_fr_date_invalid_iso_format():
    with pytest.raises(ScoValueError):
        convert_fr_date("2022-02-30T00:00:00")


# ---- Heures humaines -> ISO8601


def test_heure_to_iso8601_full_time():
    assert heure_to_iso8601("16:01:02") == "16:01:02"


def test_heure_to_iso8601_hour_minute():
    assert heure_to_iso8601("16:03") == "16:03:00"


def test_heure_to_iso8601_hour_only():
    assert heure_to_iso8601("16") == "16:00:00"


def test_heure_to_iso8601_hour_h():
    assert heure_to_iso8601("16h") == "16:00:00"


def test_heure_to_iso8601_single_digit():
    assert heure_to_iso8601("1:2:3") == "01:02:03"


def test_heure_to_iso8601_invalid_input():
    with pytest.raises(ValueError):
        heure_to_iso8601("invalid")


def test_heure_to_iso8601_datetime_time():
    time_obj = datetime.time(16, 1, 2)
    assert heure_to_iso8601(time_obj) == "16:01:02"


def test_heure_to_iso8601_null():
    assert heure_to_iso8601("") == ""


# Run the tests
if __name__ == "__main__":
    pytest.main()
